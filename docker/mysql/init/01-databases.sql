CREATE DATABASE IF NOT EXISTS `user_auth_test`;

CREATE USER `user_auth_test_user`@'%' IDENTIFIED BY 'changeme';
GRANT ALL PRIVILEGES ON `user_auth_test`.* TO 'user_auth_test_user'@'%';
