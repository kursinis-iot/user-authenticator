<?php

namespace Database\Factories;

use App\Models\InvalidatedRefreshToken;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class InvalidatedRefreshTokenFactory extends Factory
{
    protected $model = InvalidatedRefreshToken::class;

    public function definition(): array
    {
        return [
            'uuid' => Str::uuid()->toString(),
            'user_uuid' => fn() => User::query()->first()?->getKey() ?? null,
            'token' => $this->faker->unique()->text(1000),
            'created_at' => Carbon::parse($this->faker->dateTime),
            'updated_at' => Carbon::parse($this->faker->dateTime),
            'expires_at' => Carbon::parse($this->faker->dateTime),
        ];
    }
}