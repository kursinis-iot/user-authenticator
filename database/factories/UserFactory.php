<?php

namespace Database\Factories;

use App\Models\User;
use App\Service\CodeFactory\EmailVerificationCodeFactory;
use App\Service\PasswordHasher;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        /** @var EmailVerificationCodeFactory $verifyCodeFactory */
        $verifyCodeFactory = app(EmailVerificationCodeFactory::class);

        /** @var PasswordHasher $passwordHasher */
        $passwordHasher = app(PasswordHasher::class);

        $gender = $this->faker->boolean();

        return [
            'first_name' => $this->faker->firstName($gender),
            'last_name' => $this->faker->lastName($gender),
            'email' => $this->faker->unique()->safeEmail(),
            'password_hash' => $passwordHasher->hashPassword($this->faker->unique()->password()),
            'email_verification_code' => $this->faker->randomElement([null, $verifyCodeFactory->generateEmailVerifyCode()]),
            'created_at' => Carbon::parse($this->faker->dateTime),
            'updated_at' => Carbon::parse($this->faker->dateTime),
        ];
    }
}
