<?php

namespace Database\Factories;

use App\Models\ForgotPasswordToken;
use App\Models\User;
use App\Service\CodeFactory\ForgotPasswordTokenCodeFactory;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class ForgotPasswordTokenFactory extends Factory
{
    protected $model = ForgotPasswordToken::class;

    public function definition(): array
    {
        /** @var ForgotPasswordTokenCodeFactory $codeGenerator */
        $codeGenerator = app(ForgotPasswordTokenCodeFactory::class);

        return [
            'user_uuid' => fn() => User::query()->first()?->getKey() ?? null,
            'token' => $codeGenerator->generateForgotPasswordToken(),
            'created_at' => Carbon::parse($this->faker->dateTime),
        ];
    }
}