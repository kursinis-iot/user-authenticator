<?php

namespace App\Service;

use App\Exceptions\InvalidCredentialsException;
use App\Exceptions\UserNotVerifiedException;
use App\Models\User;
use App\Repository\UserRepository;
use App\Service\CodeFactory\EmailVerificationCodeFactory;

class UserService
{
    public function __construct(
        protected UserRepository               $userRepository,
        protected EmailVerificationCodeFactory $verificationCodeFactory,
        protected PasswordHasher               $passwordHasher
    )
    {

    }

    public function createUser(
        string $email,
        string $firstName,
        string $lastName,
        string $password,
        string $language,
    ): User
    {
        return $this->userRepository->create(
            $email,
            $firstName,
            $lastName,
            $this->verificationCodeFactory->generateEmailVerifyCode(),
            $this->passwordHasher->hashPassword($password),
            $language
        );
    }

    /**
     * @throws InvalidCredentialsException
     * @throws UserNotVerifiedException
     */
    public function findUserByCredentials(
        string $email,
        string $password
    ): User
    {
        /** @var ?User $user */
        $user = $this->userRepository->findByEmail($email);

        if ($user === null || !$this->passwordHasher->verifyPassword($user->getAttribute('password_hash'), $password))
            throw new InvalidCredentialsException();

        if ($user->getAttribute('email_verification_code') !== null)
            throw new UserNotVerifiedException();

        return $user;
    }

    /**
     * @throws InvalidCredentialsException
     */
    public function verifyUserEmail(string $email, string $code): void
    {
        $user = $this->userRepository->findByEmailAndCode($email, $code);
        if($user === null)
            throw new InvalidCredentialsException();

        $this->userRepository->verifyEmail($user);
    }
}