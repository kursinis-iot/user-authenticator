<?php

namespace App\Service;

class PasswordHasher
{
    public function hashPassword(string $password): string
    {
        return password_hash($password, PASSWORD_ARGON2ID);
    }

    public function verifyPassword(string $hash, string $password): bool
    {
        return password_verify($password, $hash);
    }
}