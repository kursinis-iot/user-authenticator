<?php

namespace App\Service;

use App\Exceptions\ForgotPasswordInvalidEmailException;
use App\Exceptions\ForgotPasswordTokenInvalidException;
use App\Repository\ForgotPasswordTokenRepository;
use App\Repository\UserRepository;
use App\Service\CodeFactory\ForgotPasswordTokenCodeFactory;

readonly class ForgotPasswordService
{
    public function __construct(
        protected ForgotPasswordTokenRepository  $repository,
        protected ForgotPasswordTokenCodeFactory $codeFactory,
        protected UserRepository                 $userRepository,
        protected PasswordHasher                 $passwordHasher
    )
    {

    }

    public function initiateRequest(string $email): void
    {
        $user = $this->userRepository->findByEmail($email);
        if ($user === null || $user->getAttribute('email_verification_code') !== null)
            return;

        $code = $this->codeFactory->generateForgotPasswordToken();
        $this->repository->upsertTokenForUser($user->getKey(), $code);
    }

    /**
     * @throws ForgotPasswordInvalidEmailException
     * @throws ForgotPasswordTokenInvalidException
     */
    public function submitRequest(string $email, string $code, string $password): void
    {
        $token = $this->repository->findByUserEmail($email);
        if ($token === null)
            throw new ForgotPasswordInvalidEmailException();

        if ($token->getAttribute('token') !== $code)
            throw new ForgotPasswordTokenInvalidException();

        $passwordHash = $this->passwordHasher->hashPassword($password);
        $this->userRepository->updatePasswordByUuid($token->getKey(), $passwordHash);
        $this->repository->delete($token);
    }

}