<?php

namespace App\Service\UserToken;

use App\Exceptions\TokenBelongsToDeletedUserException;
use DomainException;
use Firebase\JWT\BeforeValidException;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\SignatureInvalidException;
use InvalidArgumentException;
use Kudze\AccessTokenValidator\Exception\InvalidTokenTypeException;
use UnexpectedValueException;

class Renewer
{
    public function __construct(
        protected Factory   $factory,
        protected Validator $validator
    )
    {

    }

    /**
     * @throws InvalidTokenTypeException    Provided token is access token
     * @throws InvalidArgumentException     Provided key/key-array was empty or malformed
     * @throws DomainException              Provided JWT is malformed
     * @throws UnexpectedValueException     Provided JWT was invalid
     * @throws SignatureInvalidException    Provided JWT was invalid because the signature verification failed
     * @throws BeforeValidException         Provided JWT is trying to be used before it's eligible as defined by 'nbf'
     * @throws BeforeValidException         Provided JWT is trying to be used before it's been created as defined by 'iat'
     * @throws ExpiredException             Provided JWT has since expired, as defined by the 'exp' claim
     * @throws TokenBelongsToDeletedUserException
     */
    public function renewAccessToken(string $refreshToken): string
    {
        $user = $this->validator->validateRefreshToken($refreshToken);

        return $this->factory->generateAccessTokenForUser($user);
    }
}