<?php

namespace App\Service\UserToken;

use App\Exceptions\TokenBelongsToDeletedUserException;
use App\Repository\InvalidatedRefreshTokenRepository;
use Carbon\Carbon;
use DomainException;
use Firebase\JWT\BeforeValidException;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\SignatureInvalidException;
use InvalidArgumentException;
use Kudze\AccessTokenValidator\Exception\InvalidTokenTypeException;
use UnexpectedValueException;

readonly class Invalidator
{
    public function __construct(
        protected Validator $validator,
        protected InvalidatedRefreshTokenRepository $invalidatedRefreshTokenRepository
    ) {

    }

    /**
     * @throws InvalidTokenTypeException    Provided token is access token
     * @throws InvalidArgumentException     Provided key/key-array was empty or malformed
     * @throws DomainException              Provided JWT is malformed
     * @throws UnexpectedValueException     Provided JWT was invalid
     * @throws SignatureInvalidException    Provided JWT was invalid because the signature verification failed
     * @throws BeforeValidException         Provided JWT is trying to be used before it's eligible as defined by 'nbf'
     * @throws BeforeValidException         Provided JWT is trying to be used before it's been created as defined by 'iat'
     * @throws ExpiredException             Provided JWT has since expired, as defined by the 'exp' claim
     * @throws TokenBelongsToDeletedUserException
     */
    public function invalidateRefreshToken(string $refreshToken): void
    {
        $decoded = $this->validator->decodeRefreshToken($refreshToken);

        $this->invalidatedRefreshTokenRepository->create(
            $decoded->user->uuid,
            $refreshToken,
            Carbon::createFromTimestamp($decoded->exp)
        );
    }
}