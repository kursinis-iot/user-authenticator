<?php

namespace App\Service\UserToken;

use App\Models\User;
use Firebase\JWT\JWT;
use Kudze\AccessTokenValidator\Details;

class Factory
{
    protected string $jwtPrivateKey;
    protected int $accessTtlSeconds;
    protected int $refreshTtlSeconds;

    public function __construct()
    {
        $this->jwtPrivateKey = env('JWT_PRIVATE_KEY');
        $this->accessTtlSeconds = env('JWT_TTL_SECONDS_ACCESS');
        $this->refreshTtlSeconds = env('JWT_TTL_SECONDS_REFRESH');
    }

    public function generateAccessTokenForUser(User $user): string
    {
        $issuedAt = time();
        $expiresAt = $issuedAt + $this->accessTtlSeconds;

        return JWT::encode([
            'iss' => env('APP_URL'),
            'aud' => env('APP_URL'),
            'iat' => $issuedAt,
            'nbf' => $issuedAt,
            'exp' => $expiresAt,
            'user' => $user->toArray(),
            'type' => Details::TYPE_ACCESS_TOKEN
        ], $this->jwtPrivateKey, Details::ALGO);
    }

    public function generateRefreshTokenForUser(User $user): string
    {
        $issuedAt = time();
        $expiresAt = $issuedAt + $this->refreshTtlSeconds;

        return JWT::encode([
            'iss' => env('APP_URL'),
            'aud' => env('APP_URL'),
            'iat' => $issuedAt,
            'nbf' => $issuedAt,
            'exp' => $expiresAt,
            'user' => $user->toArray(),
            'type' => Details::TYPE_REFRESH_TOKEN
        ], $this->jwtPrivateKey, Details::ALGO);
    }

    public function generateTokenPairForUser(User $user): array
    {
        return [
            'access_token' => $this->generateAccessTokenForUser($user),
            'refresh_token' => $this->generateRefreshTokenForUser($user)
        ];
    }
}