<?php

namespace App\Service\UserToken;

use App\Exceptions\TokenBelongsToDeletedUserException;
use App\Exceptions\InvalidatedRefreshTokenException;
use App\Models\User;
use App\Repository\InvalidatedRefreshTokenRepository;
use App\Repository\UserRepository;
use DomainException;
use Firebase\JWT\BeforeValidException;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Firebase\JWT\SignatureInvalidException;
use InvalidArgumentException;
use Kudze\AccessTokenValidator\Details;
use Kudze\AccessTokenValidator\Exception\InvalidTokenTypeException;
use Kudze\AccessTokenValidator\Service\AccessTokenValidator;
use stdClass;
use UnexpectedValueException;

class Validator
{
    protected readonly string $jwtPublicKey;

    public function __construct(
        protected readonly UserRepository                    $userRepository,
        protected readonly AccessTokenValidator              $accessTokenValidator,
        protected readonly InvalidatedRefreshTokenRepository $invalidatedRefreshTokenRepository
    )
    {
        $this->jwtPublicKey = env('JWT_PUBLIC_KEY');
    }

    /**
     * @throws InvalidTokenTypeException            Provided token is access token
     * @throws InvalidArgumentException             Provided key/key-array was empty or malformed
     * @throws DomainException                      Provided JWT is malformed
     * @throws UnexpectedValueException             Provided JWT was invalid
     * @throws SignatureInvalidException            Provided JWT was invalid because the signature verification failed
     * @throws BeforeValidException                 Provided JWT is trying to be used before it's eligible as defined by 'nbf'
     * @throws BeforeValidException                 Provided JWT is trying to be used before it's been created as defined by 'iat'
     * @throws ExpiredException                     Provided JWT has since expired, as defined by the 'exp' claim
     * @throws TokenBelongsToDeletedUserException   Provided JWT belongs to an deleted user.
     */
    public function validateAccessToken(string $token): User
    {
        $decodedUser = $this->accessTokenValidator->decodeUserFromAccessToken($token);

        $user = $this->userRepository->findByUuid($decodedUser->uuid);
        if ($user === null)
            throw new TokenBelongsToDeletedUserException();

        return $user;
    }

    /**
     * @throws InvalidTokenTypeException            Provided token is access token
     * @throws InvalidArgumentException             Provided key/key-array was empty or malformed
     * @throws DomainException                      Provided JWT is malformed
     * @throws UnexpectedValueException             Provided JWT was invalid
     * @throws SignatureInvalidException            Provided JWT was invalid because the signature verification failed
     * @throws BeforeValidException                 Provided JWT is trying to be used before it's eligible as defined by 'nbf'
     * @throws BeforeValidException                 Provided JWT is trying to be used before it's been created as defined by 'iat'
     * @throws ExpiredException                     Provided JWT has since expired, as defined by the 'exp' claim
     * @throws TokenBelongsToDeletedUserException   Provided JWT refresh token belongs to an deleted user.
     * @throws InvalidatedRefreshTokenException     Provided JWT refresh token has been invalidated.
     */
    public function validateRefreshToken(string $token): User
    {
        $decoded = $this->decodeRefreshToken($token);

        $user = $this->userRepository->findByUuid($decoded->user->uuid);
        if ($user === null)
            throw new TokenBelongsToDeletedUserException();

        return $user;
    }

    /**
     * @throws InvalidTokenTypeException        Provided token is access token
     * @throws InvalidArgumentException         Provided key/key-array was empty or malformed
     * @throws DomainException                  Provided JWT is malformed
     * @throws UnexpectedValueException         Provided JWT was invalid
     * @throws SignatureInvalidException        Provided JWT was invalid because the signature verification failed
     * @throws BeforeValidException             Provided JWT is trying to be used before it's eligible as defined by 'nbf'
     * @throws BeforeValidException             Provided JWT is trying to be used before it's been created as defined by 'iat'
     * @throws ExpiredException                 Provided JWT has since expired, as defined by the 'exp' claim
     * @throws InvalidatedRefreshTokenException Provided JWT refresh token has been invalidated.
     */
    public function decodeRefreshToken(string $token): stdClass
    {
        $decoded = JWT::decode($token, new Key($this->jwtPublicKey, Details::ALGO));

        if ($decoded->type !== Details::TYPE_REFRESH_TOKEN)
            throw new InvalidTokenTypeException();

        if ($this->invalidatedRefreshTokenRepository->exists($decoded->user->uuid, $token))
            throw new InvalidatedRefreshTokenException();

        return $decoded;
    }
}