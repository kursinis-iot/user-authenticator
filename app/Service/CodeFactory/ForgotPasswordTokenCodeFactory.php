<?php

namespace App\Service\CodeFactory;

class ForgotPasswordTokenCodeFactory extends CodeFactory
{
    public const int TOKEN_LENGTH = 10;

    public function generateForgotPasswordToken(): string
    {
        return $this->generateCode(self::TOKEN_LENGTH);
    }
}