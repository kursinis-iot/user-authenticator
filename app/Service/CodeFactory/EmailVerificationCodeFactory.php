<?php

namespace App\Service\CodeFactory;

class EmailVerificationCodeFactory extends CodeFactory
{
    public const int CODE_LENGTH = 10;

    public function generateEmailVerifyCode(): string
    {
        return $this->generateCode(self::CODE_LENGTH);
    }
}