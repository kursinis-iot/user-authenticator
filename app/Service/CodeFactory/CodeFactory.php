<?php

namespace App\Service\CodeFactory;

use Illuminate\Support\Str;

abstract class CodeFactory
{
    protected function generateCode(string $length): string
    {
        return Str::random($length);
    }
}