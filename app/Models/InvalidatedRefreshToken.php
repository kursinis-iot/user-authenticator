<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class InvalidatedRefreshToken extends Model
{
    use HasUuids;
    use HasFactory;

    protected $table = "invalidated_refresh_tokens";
    protected $primaryKey = "uuid";
    public $incrementing = false;
    protected $keyType = "string";

    protected $fillable = [
        'uuid',
        'user_uuid',
        'token',
        'created_at',
        'updated_at',
        'expires_at'
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_uuid', 'uuid');
    }
}