<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class User extends Model
{
    use HasUuids;
    use HasFactory;

    const string USER_LANGUAGE_LT = 'lt';
    const string USER_LANGUAGE_EN = 'en';
    const array USER_LANGUAGES = [self::USER_LANGUAGE_LT, self::USER_LANGUAGE_EN];

    protected $table = "users";
    protected $primaryKey = "uuid";
    public $incrementing = false;
    protected $keyType = "string";

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'uuid', 'first_name', 'last_name', 'email', 'password_hash', 'email_verification_code', 'language'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var string[]
     */
    protected $hidden = [
        'password_hash', 'email_verification_code'
    ];

    public function forgotPasswordToken(): HasOne
    {
        return $this->hasOne(ForgotPasswordToken::class, 'user_uuid', 'uuid');
    }

    public function invalidatedRefreshTokens(): HasMany
    {
        return $this->hasMany(InvalidatedRefreshToken::class, 'user_uuid', 'user');
    }
}
