<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ForgotPasswordToken extends Model
{
    use HasUuids;
    use HasFactory;

    protected $table = "forgot_password_tokens";
    protected $primaryKey = "user_uuid";
    public $incrementing = false;
    protected $keyType = "string";
    const null UPDATED_AT = null;

    protected $fillable = [
        'user_uuid',
        'token',
        'created_at'
    ];

    protected $hidden = [
        'token'
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_uuid', 'uuid');
    }
}