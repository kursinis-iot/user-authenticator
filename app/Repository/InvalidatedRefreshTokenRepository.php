<?php

namespace App\Repository;
use App\Models\InvalidatedRefreshToken;
use Carbon\Carbon;

class InvalidatedRefreshTokenRepository
{
    public function exists(
        string $userUuid,
        string $refreshToken
    ): bool
    {
        return InvalidatedRefreshToken::query()->where('user_uuid', $userUuid)
            ->where('token', $refreshToken)->exists();
    }

    public function create(
        string $userUuid,
        string $refreshToken,
        string $expiresAt
    ): void
    {
        InvalidatedRefreshToken::query()->create([
            'user_uuid' => $userUuid,
            'token' => $refreshToken,
            'expires_at' => $expiresAt
        ]);
    }

    public function cleanup(): int
    {
        return InvalidatedRefreshToken::query()
            ->where("expires_at", '<', Carbon::now())
            ->delete();
    }
}