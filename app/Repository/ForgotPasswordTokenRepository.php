<?php

namespace App\Repository;

use App\Models\ForgotPasswordToken;
use Carbon\Carbon;

class ForgotPasswordTokenRepository
{
    public function findByUserEmail(string $email): ?ForgotPasswordToken
    {
        /** @var ?ForgotPasswordToken */
        return ForgotPasswordToken::query()->whereRelation('user', 'email', $email)->first();
    }

    public function upsertTokenForUser(string $userUuid, string $code): void
    {
        ForgotPasswordToken::query()->upsert(
            [
                'user_uuid' => $userUuid,
                'token' => $code
            ], ['user_uuid'], ['token']
        );
    }

    public function delete(ForgotPasswordToken $token): void
    {
        ForgotPasswordToken::query()->where('user_uuid', $token->getKey())->delete();
    }

    public function cleanup(): int
    {
        return ForgotPasswordToken::query()
            ->where("created_at", '<', Carbon::now()->subHour())
            ->delete();
    }
}