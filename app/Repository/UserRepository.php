<?php

namespace App\Repository;

use App\Models\User;
use Carbon\Carbon;

class UserRepository
{
    /**
     * Finds user by uuid.
     *
     * @param string $uuid
     * @return User|null
     */
    public function findByUuid(string $uuid): ?User
    {
        /** @var ?User */
        return User::query()->find($uuid);
    }

    /**
     * Finds user by email.
     *
     * @param string $email
     * @return User|null
     */
    public function findByEmail(string $email): ?User
    {
        /** @var ?User */
        return User::query()->where('email', $email)->first();
    }

    /**
     * Finds user by email and verification code.
     *
     * @param string $email
     * @param string $code
     * @return User|null
     */
    public function findByEmailAndCode(string $email, string $code): ?User
    {
        /** @var ?User */
        return User::query()->where('email', $email)->where('email_verification_code', $code)->first();
    }

    /**
     * Deletes not verified users.
     * (Hardcoded min time = week)
     *
     * @return int
     */
    public function deleteNotVerified(): int
    {
        return User::query()->whereNotNull('email_verification_code')
            ->where("created_at", '<', Carbon::now()->subWeeks())
            ->delete();
    }

    /**
     * Creates a user.
     *
     * @param string $email
     * @param string $firstName
     * @param string $lastName
     * @param string $emailVerifyCode
     * @param string $passwordHash
     * @param string $language
     * @return User
     */
    public function create(
        string $email,
        string $firstName,
        string $lastName,
        string $emailVerifyCode,
        string $passwordHash,
        string $language,
    ): User
    {
        /** @var User */
        return User::query()->create([
            'first_name' => $firstName,
            'last_name' => $lastName,
            'email' => $email,
            'password_hash' => $passwordHash,
            'email_verification_code' => $emailVerifyCode,
            'language' => $language
        ]);
    }

    /**
     * Verifies user mail
     *
     * @param User $user
     * @return void
     */
    public function verifyEmail(
        User $user
    ): void
    {
        $user->update(['email_verification_code' => null]);
    }

    public function updatePassword(
        User   $user,
        string $passwordHash
    ): void
    {
        $this->updatePasswordByUuid($user->getKey(), $passwordHash);
    }

    public function updatePasswordByUuid(
        string $uuid,
        string $passwordHash
    ): void
    {
        User::query()->where('uuid', $uuid)->update([
            'password_hash' => $passwordHash
        ]);
    }

}