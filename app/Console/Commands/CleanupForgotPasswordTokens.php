<?php

namespace App\Console\Commands;

use App\Repository\ForgotPasswordTokenRepository;
use Kudze\LumenBaseCli\Command\AbstractCommand;
use Symfony\Component\Console\Attribute\AsCommand;

#[AsCommand(
    CleanupForgotPasswordTokens::NAME,
    'cleans up not redeemed forgot password tokens'
)]
class CleanupForgotPasswordTokens extends AbstractCommand
{
    const string NAME = 'cleanup:forgot:password:tokens';

    public function __construct(
        protected readonly ForgotPasswordTokenRepository $forgotPasswordTokenRepository
    )
    {
        parent::__construct();
    }

    public function handle(): int
    {
        parent::handle();

        $this->io->writeln("Deleting up not redeemed forgot password tokens...");
        $deleted = $this->forgotPasswordTokenRepository->cleanup();

        $this->io->writeln("Deleted $deleted tokens!");

        return self::SUCCESS;
    }
}