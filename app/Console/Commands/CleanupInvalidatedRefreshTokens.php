<?php

namespace App\Console\Commands;

use App\Repository\InvalidatedRefreshTokenRepository;
use Kudze\LumenBaseCli\Command\AbstractCommand;
use Symfony\Component\Console\Attribute\AsCommand;

#[AsCommand(
    CleanupInvalidatedRefreshTokens::NAME,
    'cleans up expired invalidated refresh tokens'
)]
class CleanupInvalidatedRefreshTokens extends AbstractCommand
{
    const string NAME = 'cleanup:tokens:refresh:invalidated';

    public function __construct(
        protected readonly InvalidatedRefreshTokenRepository $invalidatedRefreshTokenRepository
    )
    {
        parent::__construct();
    }

    public function handle(): int
    {
        parent::handle();

        $this->io->writeln("Deleting up expired invalidated refresh tokens...");
        $deleted = $this->invalidatedRefreshTokenRepository->cleanup();

        $this->io->writeln("Deleted $deleted tokens!");

        return self::SUCCESS;
    }
}