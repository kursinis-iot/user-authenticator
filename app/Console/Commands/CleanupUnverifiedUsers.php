<?php

namespace App\Console\Commands;

use App\Repository\UserRepository;
use Kudze\LumenBaseCli\Command\AbstractCommand;
use Symfony\Component\Console\Attribute\AsCommand;

#[AsCommand(
    CleanupUnverifiedUsers::NAME,
    'cleans up unverified users'
)]
class CleanupUnverifiedUsers extends AbstractCommand
{
    const string NAME = 'cleanup:unverified:users';

    public function __construct(
        protected readonly UserRepository $userRepository
    )
    {
        parent::__construct();
    }

    public function handle(): int
    {
        parent::handle();

        $this->io->writeln("Deleting up unverified users...");
        $deleted = $this->userRepository->deleteNotVerified();

        $this->io->writeln("Deleted $deleted users!");

        return self::SUCCESS;
    }
}