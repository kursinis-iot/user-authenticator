<?php

namespace App\Console;

use App\Console\Commands\CleanupForgotPasswordTokens;
use App\Console\Commands\CleanupInvalidatedRefreshTokens;
use App\Console\Commands\CleanupUnverifiedUsers;
use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        CleanupUnverifiedUsers::class,
        CleanupForgotPasswordTokens::class,
        CleanupInvalidatedRefreshTokens::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule): void
    {
        $schedule->command(CleanupUnverifiedUsers::NAME)->daily();
        $schedule->command(CleanupForgotPasswordTokens::NAME)->hourly();
        $schedule->command(CleanupInvalidatedRefreshTokens::NAME)->hourly();
    }
}
