<?php

namespace App\Http\Controllers;

use App\Exceptions\InvalidCredentialsException;
use App\Exceptions\UserNotVerifiedException;
use App\Models\User;
use App\Service\UserService;
use App\Service\UserToken\Factory;
use App\Service\UserToken\Invalidator;
use App\Service\UserToken\Renewer;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\Exists;
use Illuminate\Validation\Rules\In;
use Illuminate\Validation\ValidationException;

class AuthenticationController extends Controller
{
    const array PASSWORD_VALIDATION_RULES = ['required', 'string', 'min:8', 'max:255'];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
        protected UserService $userService
    ) {

    }

    /**
     * Upon registration we return no content response.
     *
     * @throws ValidationException
     */
    public function register(Request $request): Response
    {
        $data = $this->validate($request, [
            'email' => 'required|email|unique:users,email|min:3|max:255',
            'first_name' => 'required|string|min:3|max:63',
            'last_name' => 'required|string|min:3|max:63',
            'password' => self::PASSWORD_VALIDATION_RULES,
            'language' => ['required', 'string', new In(User::USER_LANGUAGES)],
        ]);

        $this->userService->createUser(
            $data['email'],
            $data['first_name'],
            $data['last_name'],
            $data['password'],
            $data['language']
        );

        return $this->noContentResponse();
    }

    /**
     * Upon login, we return a JWT token.
     *
     * @param Request $request
     * @param Factory $tokenFactory
     * @return JsonResponse
     * @throws ValidationException
     */
    public function login(Request $request, Factory $tokenFactory): JsonResponse
    {
        $data = $this->validate($request, [
            'email' => 'required|string',
            'password' => 'required|string'
        ]);

        try {
            $user = $this->userService->findUserByCredentials($data['email'], $data['password']);
        } catch (InvalidCredentialsException) {
            return $this->invalidCredentialsResponse("Invalid email or password!");
        } catch (UserNotVerifiedException) {
            return $this->invalidCredentialsResponse('Verify your email address first!', 'email');
        }

        //TODO: keep track of refresh tokens.
        return new JsonResponse($tokenFactory->generateTokenPairForUser($user));
    }

    /**
     * @throws ValidationException
     */
    public function verifyEmail(Request $request): Response|JsonResponse
    {
        $data = $this->validate($request, [
            'email' => ['required', 'email', (new Exists('users', 'email'))->whereNotNull('email_verification_code')],
            'code' => "required|string"
        ]);

        try {
            $this->userService->verifyUserEmail($data['email'], $data['code']);
        } catch (InvalidCredentialsException) {
            return $this->invalidResponse("Verification code is invalid!", 'code', Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return $this->noContentResponse();
    }

    /**
     * Refreshes user token.
     *
     * @param Request $request
     * @param Renewer $renewer
     * @return Response|JsonResponse
     * @throws ValidationException
     */
    public function refreshToken(Request $request, Renewer $renewer): JsonResponse
    {
        $data = $this->validate($request, [
            'refresh_token' => 'required|string'
        ]);

        try {
            $token = $renewer->renewAccessToken($data['refresh_token']);
        } catch (Exception) {
            return $this->invalidCredentialsResponse('Invalid!', 'refresh_token');
        }

        return new JsonResponse([
            'access_token' => $token,
            'refresh_token' => $data['refresh_token']
        ]);
    }

    public function logout(Request $request, Invalidator $invalidator): Response|JsonResponse
    {
        ['refresh_token' => $refreshToken] = $this->validate($request, [
            'refresh_token' => 'required|string'
        ]);

        try {
            $invalidator->invalidateRefreshToken($refreshToken);
        } catch (Exception $e) {
            return $this->invalidCredentialsResponse('Invalid!', 'refresh_token');
        }

        return $this->noContentResponse();
    }
}
