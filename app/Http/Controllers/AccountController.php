<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Repository\UserRepository;
use App\Service\PasswordHasher;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class AccountController extends Controller
{

    /**
     * @throws ValidationException
     */
    public function changePassword(Request $request, PasswordHasher $passwordHasher, UserRepository $userRepository): Response|JsonResponse
    {
        /** @var User $user */
        $user = Auth::user();

        ['old_password' => $old, 'new_password' => $new] = $this->validate($request, [
            'old_password' => AuthenticationController::PASSWORD_VALIDATION_RULES,
            'new_password' => AuthenticationController::PASSWORD_VALIDATION_RULES,
        ]);

        if (!$passwordHasher->verifyPassword($user->getAttribute('password_hash'), $old))
            return $this->invalidCredentialsResponse('incorrect', 'old_password');

        $userRepository->updatePassword($user, $passwordHasher->hashPassword($new));

        return $this->noContentResponse();
    }

}