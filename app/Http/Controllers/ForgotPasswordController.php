<?php

namespace App\Http\Controllers;

use App\Exceptions\ForgotPasswordInvalidEmailException;
use App\Exceptions\ForgotPasswordTokenInvalidException;
use App\Service\ForgotPasswordService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;

class ForgotPasswordController extends Controller
{
    public function __construct(
        protected readonly ForgotPasswordService $service
    )
    {

    }

    /**
     * @throws ValidationException
     */
    public function initiate(Request $request): Response
    {
        ['email' => $email] = $this->validate($request, [
            'email' => 'required|email'
        ]);

        $this->service->initiateRequest($email);

        return $this->noContentResponse();
    }

    /**
     * @throws ValidationException
     */
    public function submit(Request $request): Response|JsonResponse
    {
        $data = $this->validate($request, [
            'email' => 'required|email',
            'code' => 'required|string',
            'password' => AuthenticationController::PASSWORD_VALIDATION_RULES,
        ]);

        try {
            $this->service->submitRequest($data['email'], $data['code'], $data['password']);
        } catch (ForgotPasswordTokenInvalidException|ForgotPasswordInvalidEmailException) {
            return $this->invalidResponse('invalid', 'code');
        }

        return $this->noContentResponse();
    }

}