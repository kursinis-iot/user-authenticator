<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Kudze\LumenBaseController\Http\Controllers\Controller as BaseController;
use Symfony\Component\HttpFoundation\Response as HttpResponses;

class Controller extends BaseController
{
    protected function invalidCredentialsResponse(string $message, string $key = 'credentials'): JsonResponse
    {
        return $this->invalidResponse($message, $key, HttpResponses::HTTP_UNAUTHORIZED);
    }

    protected function invalidResponse(string $message, string $key, int $code = HttpResponses::HTTP_UNPROCESSABLE_ENTITY): JsonResponse
    {
        return $this->unauthorizedResponse([$key => [$message]], $code);
    }
}
