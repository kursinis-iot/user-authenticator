<?php

namespace Tests\Service;

use App\Exceptions\ForgotPasswordInvalidEmailException;
use App\Exceptions\ForgotPasswordTokenInvalidException;
use App\Models\ForgotPasswordToken;
use App\Models\User;
use App\Repository\ForgotPasswordTokenRepository;
use App\Repository\UserRepository;
use App\Service\CodeFactory\ForgotPasswordTokenCodeFactory;
use App\Service\ForgotPasswordService;
use App\Service\PasswordHasher;
use Illuminate\Support\Str;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\MockObject\Exception;
use Tests\TestCase;

class ForgotPasswordServiceTest extends TestCase
{
    protected ForgotPasswordService $instance;

    protected ForgotPasswordTokenRepository $repositoryMock;
    protected ForgotPasswordTokenCodeFactory $codeFactoryMock;
    protected UserRepository $userRepositoryMock;
    protected PasswordHasher $passwordHasher;

    /**
     * @throws Exception
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->userRepositoryMock = $this->createMock(UserRepository::class);
        $this->codeFactoryMock = $this->createMock(ForgotPasswordTokenCodeFactory::class);
        $this->repositoryMock = $this->createMock(ForgotPasswordTokenRepository::class);
        $this->passwordHasher = $this->createMock(PasswordHasher::class);

        $this->app->instance(UserRepository::class, $this->userRepositoryMock);
        $this->app->instance(ForgotPasswordTokenCodeFactory::class, $this->codeFactoryMock);
        $this->app->instance(ForgotPasswordTokenRepository::class, $this->repositoryMock);
        $this->app->instance(PasswordHasher::class, $this->passwordHasher);

        $this->instance = app(ForgotPasswordService::class);
    }

    public static function provideInitiateRequest(): array
    {
        return [
            '#1 => random data' => [
                'test@gmail.com'
            ]
        ];
    }

    #[DataProvider('provideInitiateRequest')]
    public function testInitiateRequest_wrongEmail(string $email)
    {
        $this->userRepositoryMock->expects($this->once())->method('findByEmail')
            ->with($email)
            ->willReturn(null);

        $this->codeFactoryMock->expects($this->never())->method('generateForgotPasswordToken');
        $this->instance->initiateRequest($email);
    }

    #[DataProvider('provideInitiateRequest')]
    public function testInitiateRequest_unverifiedEmail(string $email)
    {
        $user = new User([
            'email' => $email,
            'email_verification_code' => 'abcd'
        ]);
        $this->userRepositoryMock->expects($this->once())->method('findByEmail')
            ->with($email)
            ->willReturn($user);

        $this->codeFactoryMock->expects($this->never())->method('generateForgotPasswordToken');
        $this->instance->initiateRequest($email);
    }

    #[DataProvider('provideInitiateRequest')]
    public function testInitiateRequest_valid(string $email)
    {
        $user = new User([
            'uuid' => Str::uuid()->toString(),
            'email' => $email
        ]);

        $this->userRepositoryMock->expects($this->once())->method('findByEmail')
            ->with($email)
            ->willReturn($user);

        $code = 'abcd';
        $this->codeFactoryMock->expects($this->once())->method('generateForgotPasswordToken')
            ->willReturn($code);

        $this->repositoryMock->expects($this->once())->method('upsertTokenForUser')
            ->with($user->getKey(), $code);

        $this->instance->initiateRequest($email);
    }

    public static function provideSubmitRequest(): array
    {
        return [
            '#1 => random data' => [
                'test@gmail.com',
                'abcd',
                'testtest'
            ]
        ];
    }

    /**
     * @throws ForgotPasswordTokenInvalidException
     * @throws ForgotPasswordInvalidEmailException
     */
    #[DataProvider('provideSubmitRequest')]
    public function testSubmitRequest_invalidEmail(string $email, string $code, string $password)
    {
        $this->repositoryMock->expects($this->once())->method('findByUserEmail')
            ->with($email)
            ->willReturn(null);

        $this->expectException(ForgotPasswordInvalidEmailException::class);
        $this->instance->submitRequest($email, $code, $password);
    }

    /**
     * @throws ForgotPasswordTokenInvalidException
     * @throws ForgotPasswordInvalidEmailException
     */
    #[DataProvider('provideSubmitRequest')]
    public function testSubmitRequest_invalidToken(string $email, string $code, string $password)
    {
        $token = ForgotPasswordToken::factory()->makeOne([
            'token' => 'somethingE'
        ]);

        $this->repositoryMock->expects($this->once())->method('findByUserEmail')
            ->with($email)
            ->willReturn($token);

        $this->expectException(ForgotPasswordTokenInvalidException::class);
        $this->instance->submitRequest($email, $code, $password);
    }

    /**
     * @throws ForgotPasswordTokenInvalidException
     * @throws ForgotPasswordInvalidEmailException
     */
    #[DataProvider('provideSubmitRequest')]
    public function testSubmitRequest_valid(string $email, string $code, string $password)
    {
        $token = ForgotPasswordToken::factory()->makeOne([
            'user_uuid' => '1234',
            'token' => $code
        ]);

        $this->repositoryMock->expects($this->once())->method('findByUserEmail')
            ->with($email)
            ->willReturn($token);

        $passwordHash = 'somehash';
        $this->passwordHasher->expects($this->once())->method('hashPassword')
            ->with($password)
            ->willReturn($passwordHash);

        $this->userRepositoryMock->expects($this->once())->method('updatePasswordByUuid')
            ->with($token->getKey(), $passwordHash);

        $this->repositoryMock->expects($this->once())->method('delete')
            ->with($token);

        $this->instance->submitRequest($email, $code, $password);
    }
}