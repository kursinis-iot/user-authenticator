<?php

namespace Tests\Service\UserToken;

use App\Exceptions\TokenBelongsToDeletedUserException;
use App\Models\User;
use Kudze\AccessTokenValidator\Exception\InvalidTokenTypeException;
use PHPUnit\Framework\Attributes\DataProvider;

class FactoryTest extends TestCase
{
    protected function tearDown(): void
    {
        User::query()->delete();
    }

    public static function provideGenerateAccessTokenForUser(): array
    {
        return [
            '#1 random user from 3 users' => [
                [['uuid' => '1'], [], []],
                '1'
            ]
        ];
    }

    /**
     * @throws TokenBelongsToDeletedUserException
     * @throws InvalidTokenTypeException
     */
    #[DataProvider('provideGenerateAccessTokenForUser')]
    public function testGenerateAccessTokenForUser(array $usersData, string $userUuid): void
    {
        User::factory()->createMany($usersData);
        /** @var User $user */
        $user = User::query()->find($userUuid);

        $instance = self::getFactoryInstance();
        $token = $instance->generateAccessTokenForUser($user);

        $validator = self::getValidatorInstance();
        $returnedUser = $validator->validateAccessToken($token);

        $this->assertEquals($user, $returnedUser);
    }

    public static function provideGenerateRefreshTokenForUser(): array
    {
        return [
            '#1 random user from 3 users' => [
                [['uuid' => '1'], [], []],
                '1'
            ]
        ];
    }

    /**
     * @throws TokenBelongsToDeletedUserException
     * @throws InvalidTokenTypeException
     */
    #[DataProvider('provideGenerateRefreshTokenForUser')]
    public function testGenerateRefreshTokenForUser(array $usersData, string $userUuid): void
    {
        User::factory()->createMany($usersData);
        /** @var User $user */
        $user = User::query()->find($userUuid);

        $instance = self::getFactoryInstance();
        $token = $instance->generateRefreshTokenForUser($user);

        $validator = self::getValidatorInstance();
        $returnedUser = $validator->validateRefreshToken($token);

        $this->assertEquals($user, $returnedUser);
    }

    public static function provideGenerateTokenPairForUser(): array
    {
        return [
            '#1 random user from 3 users' => [
                [['uuid' => '1'], [], []],
                '1'
            ]
        ];
    }

    /**
     * @throws TokenBelongsToDeletedUserException
     * @throws InvalidTokenTypeException
     */
    #[DataProvider('provideGenerateTokenPairForUser')]
    public function testGenerateTokenPairForUser(array $usersData, string $userUuid): void
    {
        User::factory()->createMany($usersData);
        /** @var User $user */
        $user = User::query()->find($userUuid);

        $instance = self::getFactoryInstance();
        ['access_token' => $accessToken, 'refresh_token' => $refreshToken] = $instance->generateTokenPairForUser($user);

        $validator = self::getValidatorInstance();
        $this->assertEquals($user, $validator->validateAccessToken($accessToken));
        $this->assertEquals($user, $validator->validateRefreshToken($refreshToken));
    }

}