<?php

namespace Tests\Service\UserToken;

use App\Exceptions\InvalidatedRefreshTokenException;
use App\Exceptions\TokenBelongsToDeletedUserException;
use App\Models\InvalidatedRefreshToken;
use App\Models\User;
use Carbon\Carbon;
use Firebase\JWT\BeforeValidException;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;
use Firebase\JWT\SignatureInvalidException;
use Kudze\AccessTokenValidator\Exception\InvalidTokenTypeException;
use UnexpectedValueException;

class ValidatorRefreshTest extends TestCase
{
    protected function tearDown(): void
    {
        User::query()->delete();

        JWT::$timestamp = null;
    }

    /**
     * With expired token should raise ExpiredException
     *
     * @throws InvalidTokenTypeException
     * @throws TokenBelongsToDeletedUserException
     */
    public function testExpiredToken()
    {
        $instance = self::getValidatorInstance();

        $this->expectException(ExpiredException::class);

        JWT::$timestamp = Carbon::parse('2024-02-12 23:14:00')->getTimestamp();
        $instance->validateRefreshToken(self::getValidRefreshToken());
    }

    /**
     * With future token should raise BeforeValidException
     *
     * @throws InvalidTokenTypeException
     * @throws TokenBelongsToDeletedUserException
     */
    public function testFutureToken()
    {
        $instance = self::getValidatorInstance();

        $this->expectException(BeforeValidException::class);

        JWT::$timestamp = Carbon::parse('2024-02-10 23:14:00')->getTimestamp();
        $instance->validateRefreshToken(self::getValidRefreshToken());
    }

    /**
     * With invalid token signature should raise SignatureInvalidException
     *
     * @return void
     * @throws InvalidTokenTypeException
     * @throws TokenBelongsToDeletedUserException
     */
    public function testInvalidRefreshToken()
    {
        $instance = self::getValidatorInstance();

        $this->expectException(SignatureInvalidException::class);

        JWT::$timestamp = Carbon::parse('2024-02-11 23:14:00')->getTimestamp();
        $instance->validateRefreshToken(self::getInvalidRefreshToken());
    }

    /**
     * With valid refresh token we should operate.
     *
     * @return void
     * @throws InvalidTokenTypeException
     * @throws TokenBelongsToDeletedUserException
     */
    public function testValidRefreshToken()
    {
        $user = User::factory()->create(['uuid' => '9b4fad57-0181-419a-8427-7901ee3a1ce9']);
        $instance = self::getValidatorInstance();

        JWT::$timestamp = Carbon::parse('2024-02-11 23:14:00')->getTimestamp();
        $returnedUser = $instance->validateRefreshToken(self::getValidRefreshToken());

        $this->assertSame($user->getKey(), $returnedUser->getKey());
    }

    /**
     * With valid access token we should raise InvalidTokenTypeException
     *
     * @return void
     * @throws InvalidTokenTypeException
     * @throws TokenBelongsToDeletedUserException
     */
    public function testValidAccessToken()
    {
        $instance = self::getValidatorInstance();

        $this->expectException(InvalidTokenTypeException::class);

        JWT::$timestamp = Carbon::parse('2024-02-11 23:14:00')->getTimestamp();
        $instance->validateRefreshToken(self::getValidAccessToken());
    }

    /**
     * With invalid jwt token we should get UnexpectedValueException.
     *
     * @throws InvalidTokenTypeException
     * @throws TokenBelongsToDeletedUserException
     */
    public function testInvalidToken()
    {
        $instance = self::getValidatorInstance();

        $this->expectException(UnexpectedValueException::class);
        $instance->validateRefreshToken('i am not valid token');
    }

    /**
     * With deleted user we should raise TokenBelongsToDeletedUserException
     *
     * @throws InvalidTokenTypeException
     * @throws TokenBelongsToDeletedUserException
     */
    public function testWithDeletedUser()
    {
        $instance = self::getValidatorInstance();

        $this->expectException(TokenBelongsToDeletedUserException::class);

        JWT::$timestamp = Carbon::parse('2024-02-11 23:14:00')->getTimestamp();
        $instance->validateRefreshToken(self::getValidRefreshToken());
    }

    /**
     * With valid access token we should raise InvalidTokenTypeException
     *
     * @return void
     * @throws InvalidTokenTypeException
     * @throws TokenBelongsToDeletedUserException
     */
    public function testInvalidatedRefreshToken()
    {
        $refreshToken = self::getValidRefreshToken();
        
        User::factory()->create(['uuid' => '9b4fad57-0181-419a-8427-7901ee3a1ce9']);
        InvalidatedRefreshToken::factory()->create(['user_uuid' => '9b4fad57-0181-419a-8427-7901ee3a1ce9', 'token' => $refreshToken]);
        $instance = self::getValidatorInstance();

        $this->expectException(InvalidatedRefreshTokenException::class);

        JWT::$timestamp = Carbon::parse('2024-02-11 23:14:00')->getTimestamp();
        $instance->validateRefreshToken($refreshToken);
    }
}