<?php

namespace Tests\Service\UserToken;

use Tests\Traits\UsesJWT;
use Tests\TestCase as BaseTestCase;


abstract class TestCase extends BaseTestCase
{
    use UsesJWT;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();

        self::setupJwtKeys();
    }

}