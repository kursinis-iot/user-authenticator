<?php

namespace Tests\Service\UserToken;

use App\Exceptions\TokenBelongsToDeletedUserException;
use App\Models\User;
use App\Service\UserToken\Factory;
use App\Service\UserToken\Renewer;
use App\Service\UserToken\Validator;
use Kudze\AccessTokenValidator\Exception\InvalidTokenTypeException;
use PHPUnit\Framework\MockObject\Exception;

class RenewerTest extends TestCase
{
    protected Factory $factoryMock;
    protected Validator $validatorMock;
    protected Renewer $instance;

    protected function setUp(): void
    {
        parent::setUp();

        $this->factoryMock = $this->createMock(Factory::class);
        $this->validatorMock = $this->createMock(Validator::class);

        $this->app->instance(Factory::class, $this->factoryMock);
        $this->app->instance(Validator::class, $this->validatorMock);

        $this->instance = app(Renewer::class);
    }

    /**
     * @throws Exception
     * @throws TokenBelongsToDeletedUserException
     * @throws InvalidTokenTypeException
     */
    public function testRenewToken()
    {
        //Configure mocks.
        $user = new User(['uuid' => '1']);
        $generatedToken = 'result';

        $this->validatorMock->expects($this->once())->method('validateRefreshToken')
            ->with('test')->willReturn($user);

        $this->factoryMock->expects($this->once())->method('generateAccessTokenForUser')
            ->with($user)->willReturn($generatedToken);

        //Run
        $returnedToken = $this->instance->renewAccessToken('test');

        $this->assertSame($generatedToken, $returnedToken);
    }
}