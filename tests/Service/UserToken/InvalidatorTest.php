<?php

namespace Tests\Service\UserToken;

use App\Repository\InvalidatedRefreshTokenRepository;
use App\Service\UserToken\Invalidator;
use App\Service\UserToken\Validator;
use Carbon\Carbon;
use stdClass;

class InvalidatorTest extends TestCase
{
    protected Validator $validatorMock;
    protected InvalidatedRefreshTokenRepository $invalidatedRefreshTokenRepositoryMock;
    protected Invalidator $instance;

    protected function setUp(): void
    {
        parent::setUp();
        
        $this->validatorMock = $this->createMock(Validator::class);
        $this->invalidatedRefreshTokenRepositoryMock = $this->createMock(InvalidatedRefreshTokenRepository::class);

        $this->app->instance(Validator::class, $this->validatorMock);
        $this->app->instance(InvalidatedRefreshTokenRepository::class, $this->invalidatedRefreshTokenRepositoryMock);

        $this->instance = app(Invalidator::class);
    }

    public function testInvalidate(): void
    {
        //Configure mocks.
        $decodedToken = new stdClass();
        $decodedToken->user = new stdClass();
        $decodedToken->user->uuid = '1';
        $decodedToken->exp = Carbon::now()->addHour()->getTimestamp();

        $this->validatorMock->expects($this->once())->method('decodeRefreshToken')
            ->with('test')->willReturn($decodedToken);

        $this->invalidatedRefreshTokenRepositoryMock->expects($this->once())->method('create')
            ->with(
                $decodedToken->user->uuid,
                'test',
                Carbon::createFromTimestamp($decodedToken->exp)
            );

        //Run
        $this->instance->invalidateRefreshToken('test');
    }
}