<?php

namespace Tests\Service;

use App\Exceptions\InvalidCredentialsException;
use App\Exceptions\UserNotVerifiedException;
use App\Models\User;
use App\Repository\UserRepository;
use App\Service\CodeFactory\EmailVerificationCodeFactory;
use App\Service\PasswordHasher;
use App\Service\UserService;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\MockObject\Exception;
use Tests\TestCase;

class UserServiceTest extends TestCase
{
    protected UserService $instance;
    protected UserRepository $userRepositoryMock;
    protected EmailVerificationCodeFactory $emailVerificationCodeFactoryMock;
    protected PasswordHasher $passwordHasherMock;

    /**
     * @throws Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        //Add mocks.
        $this->userRepositoryMock = $this->createMock(UserRepository::class);
        $this->emailVerificationCodeFactoryMock = $this->createMock(EmailVerificationCodeFactory::class);
        $this->passwordHasherMock = $this->createMock(PasswordHasher::class);

        $this->app->instance(UserRepository::class, $this->userRepositoryMock);
        $this->app->instance(EmailVerificationCodeFactory::class, $this->emailVerificationCodeFactoryMock);
        $this->app->instance(PasswordHasher::class, $this->passwordHasherMock);

        $this->instance = app(UserService::class);
    }

    public static function provideCreateUser(): array
    {
        return [
            '#1 random data' => [
                'test@gmail.com',
                'first',
                'last',
                'password',
                User::USER_LANGUAGE_EN,
            ]
        ];
    }

    /**
     * @throws Exception
     */
    #[DataProvider('provideCreateUser')]
    public function testCreateUser(
        string $email,
        string $firstName,
        string $lastName,
        string $password,
        string $language
    )
    {
        //Configure mocks.
        //generateEmailVerifyCode should be triggered
        $emailVerifyCode = 'a1b2'; //will return this code.
        $this->emailVerificationCodeFactoryMock->expects($this->once())->method('generateEmailVerifyCode')->willReturn($emailVerifyCode);

        //hashPassword should be triggered with passed in password.
        $passwordHash = 'password_hash'; //will return this hash.
        $this->passwordHasherMock->expects($this->once())->method('hashPassword')->with($password)->willReturn($passwordHash);

        //createUser should be triggered with following args
        $this->userRepositoryMock->expects($this->once())->method('create')->with($email, $firstName, $lastName, $emailVerifyCode, $passwordHash, $language);

        //Trigger method.
        $this->instance->createUser($email, $firstName, $lastName, $password, $language);
    }

    public static function provideFindUserByCredentials(): array
    {
        return [
            '#1 random data' => [
                'test@gmail.com',
                'first',
                'last',
                'password'
            ]
        ];
    }

    /**
     * @throws Exception
     * @throws InvalidCredentialsException
     * @throws UserNotVerifiedException
     */
    #[DataProvider('provideFindUserByCredentials')]
    public function testFindUserByCredentialsWrongEmail(string $email, string $password)
    {
        //Configure mocks.
        //First it should search for user by email, lets say it was invalid.
        $user = null;
        $this->userRepositoryMock->expects($this->once())->method('findByEmail')->with($email)->willReturn($user);

        //Then lets just verify that verifyPassword was not triggered in this case.
        $this->passwordHasherMock->expects($this->never())->method('verifyPassword');

        //Should raise InvalidCredentialsException
        $this->expectException(InvalidCredentialsException::class);

        //Trigger method.
        $this->instance->findUserByCredentials($email, $password);
    }

    /**
     * @throws Exception
     * @throws InvalidCredentialsException
     * @throws UserNotVerifiedException
     */
    #[DataProvider('provideFindUserByCredentials')]
    public function testFindUserByCredentialsWrongPassword(string $email, string $password)
    {
        //Configure mocks.
        //First it should search for user by email, lets say it was valid and verified.
        $passwordHash = 'hash';
        $user = new User([
            'email' => $email,
            'password_hash' => $passwordHash,
            'email_verification_code' => null
        ]);

        $this->userRepositoryMock->expects($this->once())->method('findByEmail')
            ->with($email)->willReturn($user);

        //Then it should trigger verifyPassword and let's say its incorrect password.
        $this->passwordHasherMock->expects($this->once())->method('verifyPassword')
            ->with($passwordHash, $password)->willReturn(false);

        //Should raise InvalidCredentialsException
        $this->expectException(InvalidCredentialsException::class);

        //Trigger method.
        $this->instance->findUserByCredentials($email, $password);
    }

    /**
     * @throws Exception
     * @throws InvalidCredentialsException
     * @throws UserNotVerifiedException
     */
    #[DataProvider('provideFindUserByCredentials')]
    public function testFindUserByCredentialsWrongPasswordNotVerified(string $email, string $password)
    {
        //Configure mocks.
        //First it should search for user by email, lets say it was valid, but not verified.
        $passwordHash = 'hash';
        $user = new User([
            'email' => $email,
            'password_hash' => $passwordHash,
            'email_verification_code' => 'a1b2'
        ]);

        $this->userRepositoryMock->expects($this->once())->method('findByEmail')
            ->with($email)->willReturn($user);

        //Then it should trigger verifyPassword and let say its incorrect password.
        $this->passwordHasherMock->expects($this->once())->method('verifyPassword')
            ->with($passwordHash, $password)->willReturn(false);

        //Should raise InvalidCredentialsException
        $this->expectException(InvalidCredentialsException::class);

        //Trigger method.
        $this->instance->findUserByCredentials($email, $password);
    }

    /**
     * @throws Exception
     * @throws InvalidCredentialsException
     * @throws UserNotVerifiedException
     */
    #[DataProvider('provideFindUserByCredentials')]
    public function testFindUserByCredentialsNotVerified(string $email, string $password)
    {
        //Configure mocks.
        //First it should search for user by email, lets say it was valid, but not verified.
        $passwordHash = 'hash';
        $user = new User([
            'email' => $email,
            'password_hash' => $passwordHash,
            'email_verification_code' => 'a1b2'
        ]);

        $this->userRepositoryMock->expects($this->once())->method('findByEmail')
            ->with($email)->willReturn($user);

        //Then it should trigger verifyPassword and let say it was correct password.
        $this->passwordHasherMock->expects($this->once())->method('verifyPassword')
            ->with($passwordHash, $password)->willReturn(true);

        //Should raise UserNotVerifiedException
        $this->expectException(UserNotVerifiedException::class);

        //Trigger method.
        $this->instance->findUserByCredentials($email, $password);
    }

    /**
     * @throws Exception
     * @throws InvalidCredentialsException
     * @throws UserNotVerifiedException
     */
    #[DataProvider('provideFindUserByCredentials')]
    public function testFindUserByCredentialsValid(string $email, string $password)
    {
        //Configure mocks.
        //First it should search for user by email, lets say it was valid.
        $passwordHash = 'hash';
        $user = new User([
            'email' => $email,
            'password_hash' => $passwordHash,
            'email_verification_code' => null
        ]);

        $this->userRepositoryMock->expects($this->once())->method('findByEmail')
            ->with($email)->willReturn($user);

        //Then it should trigger verifyPassword and let's say it was correct password.
        $this->passwordHasherMock->expects($this->once())->method('verifyPassword')
            ->with($passwordHash, $password)->willReturn(true);

        //Trigger method.
        $returnedUser = $this->instance->findUserByCredentials($email, $password);

        //returned user should be same instance what repository gave.
        $this->assertSame($user, $returnedUser);
    }

    public static function provideVerifyUserEmail(): array
    {
        return [
            '#1 random data' => [
                'test@gmail.com',
                'a1b2'
            ]
        ];
    }

    /**
     * @throws Exception
     * @throws InvalidCredentialsException
     */
    #[DataProvider('provideVerifyUserEmail')]
    public function testVerifyUserEmailValid(string $email, string $code)
    {
        //Configure mocks.
        //findUserByEmailAndCode should be triggered once, lets say it return valid user.
        $user = new User([
            'uuid' => '1',
            'email' => $email,
            'email_verification_code' => $code
        ]);

        $this->userRepositoryMock->expects($this->once())->method('findByEmailAndCode')->with($email, $code)->willReturn($user);

        //Then the service should activate the user.
        $this->userRepositoryMock->expects($this->once())->method('verifyEmail')->with($user);

        //Trigger method.
        $this->instance->verifyUserEmail($email, $code);
    }

    /**
     * @throws Exception
     * @throws InvalidCredentialsException
     */
    #[DataProvider('provideVerifyUserEmail')]
    public function testVerifyUserEmailInvalid(string $email, string $code)
    {
        //Configure mocks.
        //findUserByEmailAndCode should be triggered once, lets say it returns null.
        $user = null;
        $this->userRepositoryMock->expects($this->once())->method('findByEmailAndCode')->with($email, $code)->willReturn($user);

        //Then the service should activate the user.
        $this->userRepositoryMock->expects($this->never())->method('verifyEmail');

        //It should throw InvalidCredentialsException
        $this->expectException(InvalidCredentialsException::class);

        //Trigger method.
        $this->instance->verifyUserEmail($email, $code);
    }
}