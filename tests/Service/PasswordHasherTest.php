<?php

namespace Tests\Service;

use App\Service\PasswordHasher;
use Tests\TestCase;

class PasswordHasherTest extends TestCase
{
    public static function getInstance(): PasswordHasher
    {
        return app(PasswordHasher::class);
    }

    public function test()
    {
        $instance = self::getInstance();

        $hash = $instance->hashPassword('test');
        $this->assertNotSame('test', $hash);

        $this->assertFalse($instance->verifyPassword($hash, 'testas'));
        $this->assertFalse($instance->verifyPassword($hash, 'random'));
        $this->assertTrue($instance->verifyPassword($hash, 'test'));
    }

}