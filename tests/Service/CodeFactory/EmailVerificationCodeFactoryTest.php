<?php

namespace Tests\Service\CodeFactory;

use App\Service\CodeFactory\EmailVerificationCodeFactory;
use Tests\TestCase;

class EmailVerificationCodeFactoryTest extends TestCase
{
    public static function getInstance(): EmailVerificationCodeFactory
    {
        return app(EmailVerificationCodeFactory::class);
    }

    public function testUnique()
    {
        $instance = $this->getInstance();

        $code1 = $instance->generateEmailVerifyCode();
        $code2 = $instance->generateEmailVerifyCode();
        $this->assertNotSame($code1, $code2);
    }

    public function testLength()
    {
        $instance = $this->getInstance();

        $code = $instance->generateEmailVerifyCode();
        $this->assertSame(EmailVerificationCodeFactory::CODE_LENGTH, strlen($code));
    }
}