<?php

namespace Tests\Service\CodeFactory;

use App\Service\CodeFactory\ForgotPasswordTokenCodeFactory;
use Tests\TestCase;

class ForgotPasswordTokenCodeFactoryTest extends TestCase
{
    public static function getInstance(): ForgotPasswordTokenCodeFactory
    {
        return app(ForgotPasswordTokenCodeFactory::class);
    }

    public function testUnique()
    {
        $instance = $this->getInstance();

        $code1 = $instance->generateForgotPasswordToken();
        $code2 = $instance->generateForgotPasswordToken();
        $this->assertNotSame($code1, $code2);
    }

    public function testLength()
    {
        $instance = $this->getInstance();

        $code = $instance->generateForgotPasswordToken();
        $this->assertSame(ForgotPasswordTokenCodeFactory::TOKEN_LENGTH, strlen($code));
    }
}