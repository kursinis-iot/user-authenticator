<?php

namespace Tests\Repository;

use App\Models\InvalidatedRefreshToken;
use App\Models\User;
use App\Repository\InvalidatedRefreshTokenRepository;
use Carbon\Carbon;
use PHPUnit\Framework\Attributes\DataProvider;
use Tests\TestCase;
use Tests\Traits\InteractsWithDatabase;

class InvalidatedRefreshTokenRepositoryTest extends TestCase
{
    use InteractsWithDatabase;

    protected function tearDown(): void
    {
        User::query()->delete();

        parent::tearDown();
    }

    public static function getInstance(): InvalidatedRefreshTokenRepository
    {
        return app(InvalidatedRefreshTokenRepository::class);
    }

    public static function provideUsers(): array
    {
        return [
            ['uuid' => '1', 'email' => 'test1@gmail.com'],
            ['uuid' => '2', 'email' => 'test2@gmail.com'],
            ['uuid' => '3', 'email' => 'test3@gmail.com'],
        ];
    }

    public static function provideExists(): array
    {
        return [
            '#1 => should not find if empty' => [
                self::provideUsers(),
                [],
                '1',
                'test',
                false
            ],
            '#2 => should not find if at other user' => [
                self::provideUsers(),
                [
                    [
                        'user_uuid' => '2',
                        'token' => 'test'
                    ]
                ],
                '1',
                'test',
                false
            ],
            '#3 => should not find if token is different' => [
                self::provideUsers(),
                [
                    [
                        'user_uuid' => '1',
                        'token' => 'somethingelse'
                    ]
                ],
                '1',
                'test',
                false
            ],
            '#4 => should find' => [
                self::provideUsers(),
                [
                    [
                        'user_uuid' => '1',
                        'token' => 'test'
                    ]
                ],
                '1',
                'test',
                true
            ],
        ];
    }

    #[DataProvider('provideExists')]
    public function testExists(array $usersData, array $invalidatedRefreshTokensData, string $userUuid, string $refreshToken, bool $assertFound)
    {
        User::factory()->createMany($usersData);
        InvalidatedRefreshToken::factory()->createMany($invalidatedRefreshTokensData);

        $instance = $this->getInstance();
        $found = $instance->exists($userUuid, $refreshToken);

        $this->assertSame($assertFound, $found);
    }

    public static function provideCreate(): array
    {
        return [
            '#1 => random data' => [
                self::provideUsers(),
                '1',
                'test',
                Carbon::now()->toDateTimeString()
            ]
        ];
    }

    #[DataProvider('provideCreate')]
    public function testCreate(array $usersData, string $userUuid, string $refreshToken, string $expiresAt)
    {
        User::factory()->createMany($usersData);
       
        $instance = $this->getInstance();
        $instance->create($userUuid, $refreshToken, $expiresAt);

        $this->assertDatabaseHas('invalidated_refresh_tokens', [
            'user_uuid' => $userUuid,
            'token' => $refreshToken,
            'expires_at' => $expiresAt
        ]);
    }

    public static function provideCleanup(): array
    {
        $now = Carbon::now();

        return [
            '#1 should only delete invalidated refresh tokens that are expired' => [
                self::provideUsers(),
                [
                    ['user_uuid' => '1', 'token' => 'A1B2', 'expires_at' => $now->copy()->addMinutes(30)],
                    ['user_uuid' => '2', 'token' => 'A2B2', 'expires_at' => $now->copy()->subMinutes(1)],
                    ['user_uuid' => '3', 'token' => 'ABC', 'expires_at' => $now->copy()->subMinutes(90)],
                ],
                2,
                [
                    ['user_uuid' => '1'],
                ],
                [
                    ['user_uuid' => '2'],
                    ['user_uuid' => '3'],
                ]
            ]
        ];
    }

    #[DataProvider('provideCleanup')]
    public function testCleanup(array $usersData, array $tokenData, int $deletedCount, array $databaseHas = [], array $databaseMissing = [])
    {
        User::factory()->createMany($usersData);
        InvalidatedRefreshToken::factory()->createMany($tokenData);

        $instance = self::getInstance();
        $count = $instance->cleanup();

        $this->assertSame($deletedCount, $count);

        foreach ($databaseHas as $row)
            $this->assertDatabaseHas('invalidated_refresh_tokens', $row);

        foreach ($databaseMissing as $row)
            $this->assertDatabaseMissing('invalidated_refresh_tokens', $row);
    }
}