<?php

namespace Tests\Repository;

use App\Models\User;
use App\Repository\UserRepository;
use Carbon\Carbon;
use Illuminate\Support\Str;
use PHPUnit\Framework\Attributes\DataProvider;
use Tests\TestCase;
use Tests\Traits\InteractsWithDatabase;

class UserRepositoryTest extends TestCase
{
    use InteractsWithDatabase;

    protected function tearDown(): void
    {
        User::query()->delete();
    }

    public static function getInstance(): UserRepository
    {
        return app(UserRepository::class);
    }

    public static function provideFindUserByUuid(): array
    {
        $validUuid = Str::uuid()->toString();

        return [
            '#1 not valid uuid' => [
                [],
                Str::uuid()->toString(),
                null,
            ],
            '#2 not valid uuid (with 3 users)' => [
                [[], [], []],
                Str::uuid()->toString(),
                null,
            ],
            '#3 valid uuid' => [
                [['uuid' => $validUuid]],
                $validUuid,
                $validUuid
            ],
            '#4 valid uuid (with 4 users)' => [
                [['uuid' => $validUuid], [], [], []],
                $validUuid,
                $validUuid
            ]
        ];
    }

    #[DataProvider('provideFindUserByUuid')]
    public function testFindUserByUuid(
        array   $usersData,
        string  $uuid,
        ?string $expectedUuid,
    ): void
    {
        User::factory()->createMany($usersData);

        $instance = self::getInstance();
        $data = $instance->findByUuid($uuid);

        if ($expectedUuid === null) $this->assertNull($data);
        else $this->assertSame($expectedUuid, $data->getKey());
    }

    public static function provideUserByEmail(): array
    {
        $validUuid = Str::uuid()->toString();

        return [
            '#1 not valid email' => [
                [],
                'test@gmail.com',
                null,
            ],
            '#2 not valid email (with 3 users)' => [
                [
                    ['email' => 'invalid1@gmail.com'],
                    ['email' => 'invalid2@gmail.com'],
                    ['email' => 'invalid3@gmail.com'],
                ],
                'test@gmail.com',
                null,
            ],
            '#3 valid email' => [
                [
                    ['email' => 'valid@gmail.com', 'uuid' => $validUuid],
                ],
                'valid@gmail.com',
                $validUuid,
            ],
            '#4 valid email (with 3 users)' => [
                [
                    ['email' => 'valid@gmail.com', 'uuid' => $validUuid],
                    ['email' => 'invalid2@gmail.com'],
                    ['email' => 'invalid3@gmail.com'],
                ],
                'valid@gmail.com',
                $validUuid,
            ],
            '#5 valid email (with 3 users) (case insensitivity check)' => [
                [
                    ['email' => 'valid1@gmail.com', 'uuid' => $validUuid],
                    ['email' => 'invalid2@gmail.com'],
                    ['email' => 'invalid3@gmail.com'],
                ],
                'VALID1@gmail.com',
                $validUuid,
            ],
        ];
    }

    #[DataProvider('provideUserByEmail')]
    public function testFindUserByEmail(
        array   $usersData,
        string  $email,
        ?string $expectedUuid
    ): void
    {
        User::factory()->createMany($usersData);

        $instance = self::getInstance();
        $data = $instance->findByEmail($email);

        if ($expectedUuid === null) $this->assertNull($data);
        else $this->assertSame($expectedUuid, $data->getKey());
    }

    public static function provideUserByEmailAndCode(): array
    {
        $validUuid = Str::uuid()->toString();

        return [
            '#1 not valid email and code' => [
                [],
                'test@gmail.com',
                'a1b2',
                null,
            ],
            '#2 not valid email (with 3 users)' => [
                [
                    ['email' => 'invalid1@gmail.com'],
                    ['email' => 'invalid2@gmail.com'],
                    ['email' => 'invalid3@gmail.com'],
                ],
                'test@gmail.com',
                'a1b2',
                null,
            ],
            '#3 valid email but not code' => [
                [
                    ['email' => 'valid@gmail.com', 'email_verification_code' => 'a1b2'],
                ],
                'valid@gmail.com',
                'a2b1',
                null,
            ],
            '#4 valid email but not code (with 3 users)' => [
                [
                    ['email' => 'valid@gmail.com', 'email_verification_code' => 'a1b2'],
                    ['email' => 'invalid2@gmail.com'],
                    ['email' => 'invalid3@gmail.com'],
                ],
                'valid@gmail.com',
                'a2b1',
                null,
            ],
            '#5 valid email but not code (case sensitivity check)' => [
                [
                    ['email' => 'valid@gmail.com', 'email_verification_code' => 'a1b2'],
                ],
                'valid@gmail.com',
                'A1B2',
                null,
            ],
            '#6 valid email but not code (case sensitivity check) (with 3 emails)' => [
                [
                    ['email' => 'valid@gmail.com', 'email_verification_code' => 'a1b2'],
                    ['email' => 'invalid2@gmail.com'],
                    ['email' => 'invalid3@gmail.com'],
                ],
                'valid@gmail.com',
                'A1B2',
                null,
            ],
            '#7 valid email and code' => [
                [
                    ['email' => 'valid@gmail.com', 'email_verification_code' => 'a1b2', 'uuid' => $validUuid],
                ],
                'valid@gmail.com',
                'a1b2',
                $validUuid,
            ],
            '#8 valid email and code (with 3 users)' => [
                [
                    ['email' => 'valid@gmail.com', 'email_verification_code' => 'a1b2', 'uuid' => $validUuid],
                    ['email' => 'invalid2@gmail.com'],
                    ['email' => 'invalid3@gmail.com'],
                ],
                'valid@gmail.com',
                'a1b2',
                $validUuid,
            ],
        ];
    }

    #[DataProvider('provideUserByEmailAndCode')]
    public function testFindUserByEmailAndCode(
        array   $usersData,
        string  $email,
        string  $code,
        ?string $expectedUuid
    ): void
    {
        User::factory()->createMany($usersData);

        $instance = self::getInstance();
        $data = $instance->findByEmailAndCode($email, $code);

        if ($expectedUuid === null) $this->assertNull($data);
        else $this->assertSame($expectedUuid, $data->getKey());
    }

    public static function provideDeleteNotVerifiedUsers(): array
    {
        $now = Carbon::now();

        return [
            '#1 should not touch accounts with verified mail' => [
                [
                    ['uuid' => '1', 'email_verification_code' => null, 'created_at' => $now],
                    ['uuid' => '2', 'email_verification_code' => null, 'created_at' => $now->copy()->subWeeks(2)],
                    ['uuid' => '3', 'email_verification_code' => null, 'created_at' => $now->copy()->subWeeks(64)],
                ],
                0,
                [
                    ['uuid' => '1'],
                    ['uuid' => '2'],
                    ['uuid' => '3'],
                ],
            ],
            '#2 should only delete not verified accounts that have been pending verification for a week' => [
                [
                    ['uuid' => '1', 'email_verification_code' => 'A1B2', 'created_at' => $now],
                    ['uuid' => '2', 'email_verification_code' => 'A2B2', 'created_at' => $now->copy()->subWeeks(2)],
                    ['uuid' => '3', 'email_verification_code' => null, 'created_at' => $now->copy()->subWeeks(64)],
                ],
                1,
                [
                    ['uuid' => '1'],
                    ['uuid' => '3'],
                ],
                [
                    ['uuid' => '2'],
                ]
            ]
        ];
    }

    #[DataProvider('provideDeleteNotVerifiedUsers')]
    public function testDeleteNotVerifiedUsers(array $usersData, int $deletedCount, array $databaseHas = [], array $databaseMissing = []): void
    {
        User::factory()->createMany($usersData);

        $instance = self::getInstance();
        $count = $instance->deleteNotVerified();

        $this->assertSame($deletedCount, $count);

        foreach ($databaseHas as $row)
            $this->assertDatabaseHas('users', $row);

        foreach ($databaseMissing as $row)
            $this->assertDatabaseMissing('users', $row);
    }

    public static function provideCreateUser(): array
    {
        return [
            '#1 random data' => [
                'test@gmail.com',
                'first',
                'last',
                'a1b2',
                'passwordHash',
                User::USER_LANGUAGE_EN
            ]
        ];
    }

    #[DataProvider('provideCreateUser')]
    public function testCreateUser(
        string $email,
        string $firstName,
        string $lastName,
        string $emailVerifyCode,
        string $passwordHash,
        string $language
    ): void
    {
        $instance = self::getInstance();
        $instance->create($email, $firstName, $lastName, $emailVerifyCode, $passwordHash, $language);

        $this->assertDatabaseHas('users', [
            'email' => $email,
            'first_name' => $firstName,
            'last_name' => $lastName,
            'email_verification_code' => $emailVerifyCode,
            'password_hash' => $passwordHash,
            'language' => $language
        ]);
    }

    public static function provideVerifyUserMail(): array
    {
        return [
            '#1 should only verify passed in user' => [
                [
                    ['uuid' => '1', 'email_verification_code' => 'a1b2'],
                    ['uuid' => '2', 'email_verification_code' => 'a1b2'],
                    ['uuid' => '3', 'email_verification_code' => null],
                ],
                '1',
                [
                    ['uuid' => '1', 'email_verification_code' => null],
                    ['uuid' => '2', 'email_verification_code' => 'a1b2'],
                    ['uuid' => '3', 'email_verification_code' => null],
                ]
            ],
            '#2 should not change anything if user is already verified' => [
                [
                    ['uuid' => '1', 'email_verification_code' => null],
                ],
                '1',
                [
                    ['uuid' => '1', 'email_verification_code' => null],
                ]
            ]
        ];
    }

    #[DataProvider('provideVerifyUserMail')]
    public function testVerifyUserMail(array $usersData, string $userUuid, array $databaseHas): void
    {
        User::factory()->createMany($usersData);
        $instance = self::getInstance();

        /** @var User $user */
        $user = User::query()->find($userUuid);
        $instance->verifyEmail($user);

        foreach ($databaseHas as $row)
            $this->assertDatabaseHas('users', $row);
    }


    public static function provideTestUpdatePassword(): array
    {
        return [
            '#1 should only change password for passed in user' => [
                [
                    ['uuid' => '1', 'password_hash' => 'test'],
                    ['uuid' => '2', 'password_hash' => 'test'],
                    ['uuid' => '3', 'password_hash' => 'test'],
                ],
                '1',
                'testtest',
                [
                    ['uuid' => '1', 'password_hash' => 'testtest'],
                    ['uuid' => '2', 'password_hash' => 'test'],
                    ['uuid' => '3', 'password_hash' => 'test'],
                ]
            ]
        ];
    }

    #[DataProvider('provideTestUpdatePassword')]
    public function testUpdatePassword(array $usersData, string $userUuid, string $passwordHash, array $databaseHas)
    {
        User::factory()->createMany($usersData);
        $instance = self::getInstance();

        /** @var User $user */
        $user = User::query()->find($userUuid);
        $instance->updatePassword($user, $passwordHash);

        foreach ($databaseHas as $row)
            $this->assertDatabaseHas('users', $row);
    }

    #[DataProvider('provideTestUpdatePassword')]
    public function testUpdatePasswordByUuid(array $usersData, string $userUuid, string $passwordHash, array $databaseHas)
    {
        User::factory()->createMany($usersData);
        $instance = self::getInstance();

        /** @var User $user */
        $user = User::query()->find($userUuid);
        $instance->updatePasswordByUuid($user->getKey(), $passwordHash);

        foreach ($databaseHas as $row)
            $this->assertDatabaseHas('users', $row);
    }
}