<?php

namespace Tests\Repository;

use App\Models\ForgotPasswordToken;
use App\Models\User;
use App\Repository\ForgotPasswordTokenRepository;
use Carbon\Carbon;
use PHPUnit\Framework\Attributes\DataProvider;
use Tests\TestCase;
use Tests\Traits\InteractsWithDatabase;

class ForgotPasswordTokenRepositoryTest extends TestCase
{
    use InteractsWithDatabase;

    protected function tearDown(): void
    {
        User::query()->delete();

        parent::tearDown();
    }

    public static function getInstance(): ForgotPasswordTokenRepository
    {
        return app(ForgotPasswordTokenRepository::class);
    }

    public static function provideUsers(): array
    {
        return [
            ['uuid' => '1', 'email' => 'test1@gmail.com'],
            ['uuid' => '2', 'email' => 'test2@gmail.com'],
            ['uuid' => '3', 'email' => 'test3@gmail.com'],
        ];
    }

    public static function provideFindByUserEmail(): array
    {
        return [
            '#1 should fetch the wanted user' => [
                self::provideUsers(),
                //forgot password token data
                [
                    ['user_uuid' => '3', 'token' => 'random']
                ],
                //email
                'test3@gmail.com',
                //assert
                [
                    'user_uuid' => '3',
                    'token' => 'random'
                ]
            ],
            '#2 in case of invalid email null should be returned' => [
                self::provideUsers(),
                //forgot password token data
                [
                    ['user_uuid' => '3', 'token' => 'random']
                ],
                //email
                'test1@gmail.com',
                //assert
                null
            ]
        ];
    }

    #[DataProvider('provideFindByUserEmail')]
    public function testFindByUserEmail(
        array  $userData,
        array  $forgotPasswordData,
        string $email,
        ?array $assertAttributes
    )
    {
        User::factory()->createMany($userData);
        ForgotPasswordToken::factory()->createMany($forgotPasswordData);

        $instance = self::getInstance();

        $token = $instance->findByUserEmail($email);

        if ($assertAttributes === null) {
            $this->assertNull($token);
            return;
        }

        foreach ($assertAttributes as $key => $value)
            $this->assertSame($value, $token->getAttribute($key));
    }

    public static function provideTestUpsert(): array
    {
        return [
            '#1 should only upsert for provided user' => [
                self::provideUsers(),
                //forgot password token data
                [
                    ['user_uuid' => '3', 'token' => 'random']
                ],
                //upsert for user
                '1',
                //first upsert call set token
                'test',
                //assert has after first upsert call
                [
                    ['user_uuid' => '1', 'token' => 'test'],
                    ['user_uuid' => '3', 'token' => 'random'],
                ],
                //assert has not after first upsert call
                [
                    ['user_uuid' => '2'],
                ],
                //second upsert call set token
                'testtest',
                //assert after second upsert call
                [
                    ['user_uuid' => '1', 'token' => 'testtest'],
                    ['user_uuid' => '3', 'token' => 'random'],
                ],
                //assert has not after first upsert call
                [
                    ['user_uuid' => '2'],
                ],
                //assert pre has not
                [
                    ['user_uuid' => '1'],
                    ['user_uuid' => '2']
                ]
            ]
        ];
    }

    #[DataProvider('provideTestUpsert')]
    public function testUpsert(
        array  $userData,
        array  $forgotPasswordData,
        string $userUuid,
        string $token,
        array  $assertHas,
        array  $assertHasNot = [],
        string $token2 = null,
        array  $assertHas2 = [],
        array  $assertHasNot2 = [],
        array  $assertPreHasNot = [],
    )
    {
        User::factory()->createMany($userData);
        ForgotPasswordToken::factory()->createMany($forgotPasswordData);

        $instance = self::getInstance();

        foreach ($assertPreHasNot as $row)
            $this->assertDatabaseMissing('forgot_password_tokens', $row);

        $instance->upsertTokenForUser($userUuid, $token);

        foreach ($assertHas as $row)
            $this->assertDatabaseHas('forgot_password_tokens', $row);

        foreach ($assertHasNot as $row)
            $this->assertDatabaseMissing('forgot_password_tokens', $row);

        if ($token2 === null)
            return;

        $instance->upsertTokenForUser($userUuid, $token2);

        foreach ($assertHas2 as $row)
            $this->assertDatabaseHas('forgot_password_tokens', $row);

        foreach ($assertHasNot2 as $row)
            $this->assertDatabaseMissing('forgot_password_tokens', $row);
    }

    public static function provideDelete(): array
    {
        return [
            '#1 should delete the wanted token' => [
                self::provideUsers(),
                //forgot password token data
                [
                    ['user_uuid' => '2', 'token' => 'random'],
                    ['user_uuid' => '3', 'token' => 'random']
                ],
                //user uuid
                '2',
                //assert has
                [['user_uuid' => '3']],
                //assert has not
                [['user_uuid' => '2']]
            ],
        ];
    }

    #[DataProvider('provideDelete')]
    public function testDelete(
        array  $userData,
        array  $forgotPasswordData,
        string $userUuid,
        array  $assertHas,
        array  $assertHasNot
    )
    {
        User::factory()->createMany($userData);
        ForgotPasswordToken::factory()->createMany($forgotPasswordData);

        $instance = self::getInstance();

        /** @var ForgotPasswordToken $token */
        $token = ForgotPasswordToken::query()->findOrFail($userUuid);
        $instance->delete($token);

        foreach ($assertHas as $row)
            $this->assertDatabaseHas('forgot_password_tokens', $row);

        foreach ($assertHasNot as $row)
            $this->assertDatabaseMissing('forgot_password_tokens', $row);
    }

    public static function provideCleanup(): array
    {
        $now = Carbon::now();

        return [
            '#1 should only delete tokens that have been pending for at least a hour' => [
                self::provideUsers(),
                [
                    ['user_uuid' => '1', 'token' => 'A1B2', 'created_at' => $now->copy()->subMinutes(30)],
                    ['user_uuid' => '2', 'token' => 'A2B2', 'created_at' => $now->copy()->subMinutes(90)],
                    ['user_uuid' => '3', 'token' => 'ABC', 'created_at' => $now->copy()->subDays(64)],
                ],
                2,
                [
                    ['user_uuid' => '1'],
                ],
                [
                    ['user_uuid' => '2'],
                    ['user_uuid' => '3'],
                ]
            ]
        ];
    }

    #[DataProvider('provideCleanup')]
    public function testCleanup(array $usersData, array $tokenData, int $deletedCount, array $databaseHas = [], array $databaseMissing = [])
    {
        User::factory()->createMany($usersData);
        ForgotPasswordToken::factory()->createMany($tokenData);

        $instance = self::getInstance();
        $count = $instance->cleanup();

        $this->assertSame($deletedCount, $count);

        foreach ($databaseHas as $row)
            $this->assertDatabaseHas('forgot_password_tokens', $row);

        foreach ($databaseMissing as $row)
            $this->assertDatabaseMissing('forgot_password_tokens', $row);
    }
}