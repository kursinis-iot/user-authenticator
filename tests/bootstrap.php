<?php

use App\Console\Kernel;

$app = require __DIR__ . '/../bootstrap/app.php';
$app->boot();

$app[Kernel::class]->call('migrate');
