<?php

namespace Tests\Console\Commands;

use App\Console\Commands\CleanupUnverifiedUsers;
use App\Repository\UserRepository;
use PHPUnit\Framework\MockObject\Exception;
use Symfony\Component\Console\Command\Command;
use Tests\TestCase;

class CleanupUnverifiedUsersTest extends TestCase
{
    /**
     * @throws Exception
     */
    public function test()
    {
        $userRepositoryMock = $this->createMock(UserRepository::class);
        $this->app->instance(UserRepository::class, $userRepositoryMock);

        //Cli should trigger deleteNotVerifiedUsers.
        $userRepositoryMock->expects($this->once())->method('deleteNotVerified');

        $code = $this->artisan(CleanupUnverifiedUsers::NAME);

        //Cli should be successful.
        $this->assertSame(Command::SUCCESS, $code);
    }
}