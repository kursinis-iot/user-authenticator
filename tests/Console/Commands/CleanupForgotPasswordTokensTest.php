<?php

namespace Tests\Console\Commands;

use App\Console\Commands\CleanupForgotPasswordTokens;
use App\Repository\ForgotPasswordTokenRepository;
use PHPUnit\Framework\MockObject\Exception;
use Symfony\Component\Console\Command\Command;
use Tests\TestCase;

class CleanupForgotPasswordTokensTest extends TestCase
{
    /**
     * @throws Exception
     */
    public function test()
    {
        $tokenRepository = $this->createMock(ForgotPasswordTokenRepository::class);
        $this->app->instance(ForgotPasswordTokenRepository::class, $tokenRepository);

        //Cli should trigger cleanup.
        $tokenRepository->expects($this->once())->method('cleanup');

        $code = $this->artisan(CleanupForgotPasswordTokens::NAME);

        //Cli should be successful.
        $this->assertSame(Command::SUCCESS, $code);
    }
}