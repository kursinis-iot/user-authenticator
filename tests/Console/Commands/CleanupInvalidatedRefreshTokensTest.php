<?php

namespace Tests\Console\Commands;

use App\Console\Commands\CleanupInvalidatedRefreshTokens;
use App\Repository\InvalidatedRefreshTokenRepository;
use PHPUnit\Framework\MockObject\Exception;
use Symfony\Component\Console\Command\Command;
use Tests\TestCase;

class CleanupInvalidatedRefreshTokensTest extends TestCase
{
    /**
     * @throws Exception
     */
    public function test()
    {
        $tokenRepository = $this->createMock(InvalidatedRefreshTokenRepository::class);
        $this->app->instance(InvalidatedRefreshTokenRepository::class, $tokenRepository);

        //Cli should trigger cleanup.
        $tokenRepository->expects($this->once())->method('cleanup');

        $code = $this->artisan(CleanupInvalidatedRefreshTokens::NAME);

        //Cli should be successful.
        $this->assertSame(Command::SUCCESS, $code);
    }
}