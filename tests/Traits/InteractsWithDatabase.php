<?php

namespace Tests\Traits;

use Illuminate\Testing\Assert;
use Illuminate\Testing\Constraints\HasInDatabase;
use PHPUnit\Framework\Constraint\LogicalNot as ReverseConstraint;

//Some helper functions from laravel.
trait InteractsWithDatabase
{
    protected function assertDatabaseHas($table, array $data, $connection = null): static
    {
        Assert::assertThat(
            $table, new HasInDatabase($this->getConnection($connection), $data)
        );

        return $this;
    }

    protected function assertDatabaseMissing($table, array $data, $connection = null): static
    {
        $constraint = new ReverseConstraint(
            new HasInDatabase($this->getConnection($connection), $data)
        );

        Assert::assertThat($table, $constraint);

        return $this;
    }

    protected function getConnection($connection = null)
    {
        $database = $this->app->make('db');

        $connection = $connection ?: $database->getDefaultConnection();

        return $database->connection($connection);
    }
}
