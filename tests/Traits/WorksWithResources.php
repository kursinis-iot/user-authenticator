<?php

namespace Tests\Traits;

trait WorksWithResources
{
    protected static function getContents(string $path): string
    {
        return file_get_contents($path);
    }

    protected static function getContentsInResources(string $path): string
    {
        return self::getContents(__DIR__ . '/../_resources/' . $path);
    }

    protected static function getPayload(string $path): array
    {
        return json_decode(self::getContents($path), true);
    }

    protected static function getPayloadInResources(string $path): array
    {
        return json_decode(self::getContentsInResources($path), true);
    }
}
