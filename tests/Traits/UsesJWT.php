<?php

namespace Tests\Traits;

use App\Service\UserToken\Factory;
use App\Service\UserToken\Renewer;
use App\Service\UserToken\Validator;

trait UsesJWT
{
    use WorksWithResources;

    public static function setupJwtKeys(): void
    {
        $_ENV['JWT_PUBLIC_KEY'] = self::getContentsInResources('jwt.pub');
        $_ENV['JWT_PRIVATE_KEY'] = self::getContentsInResources('jwt.key');
    }

    protected static function getFactoryInstance(): Factory
    {
        return app(Factory::class);
    }

    protected static function getValidatorInstance(): Validator
    {
        return app(Validator::class);
    }

    protected static function getRenewerInstance(): Renewer
    {
        return app(Renewer::class);
    }

    protected static function getToken(string $path): string
    {
        return self::getContentsInResources('tokens/' . $path);
    }

    protected static function getValidAccessToken(): string
    {
        return self::getToken('access_valid.jwt');
    }

    protected static function getInvalidAccessToken(): string
    {
        return self::getToken('access_invalid.jwt');
    }

    protected static function getValidRefreshToken(): string
    {
        return self::getToken('refresh_valid.jwt');
    }

    protected static function getInvalidRefreshToken(): string
    {
        return self::getToken('refresh_invalid.jwt');
    }
}