<?php

namespace Tests\Traits;

use App\Models\User;
use App\Service\PasswordHasher;
use App\Service\UserToken\Factory;

trait UsesUsers
{
    protected static function setUpUsers(): void
    {
        /** @var PasswordHasher $passwordHasher */
        $passwordHasher = app(PasswordHasher::class);

        User::factory()->createMany([
            [
                'uuid' => '9b4fad57-0181-419a-8427-7901ee3a1ce5',
                'email' => 'test@gmail.com',
                'password_hash' => $passwordHasher->hashPassword('testtest'),
                'email_verification_code' => null
            ], 
            [
                'email' => 'test2@gmail.com',
                'password_hash' => $passwordHasher->hashPassword('testtest'),
                'email_verification_code' => 'a1b2'
            ],
            [
                'uuid' => '9b4fad57-0181-419a-8427-7901ee3a1ce9',
                'email' => 'karolis@kudze.lt',
                'first_name' => 'Karolis',
                'last_name' => 'Kraujelis',
                'password_hash' => $passwordHasher->hashPassword('testtest'),
                'email_verification_code' => null,
            ]
        ]);
    }

    protected static function deleteUsers(): void
    {
        User::query()->delete();
    }

    protected static function getValidAuthenticatedRequestHeadersForEmail(string $email): array
    {
        /** @var User $user */
        $user = User::query()->where('email', $email)->firstOrFail();

        return self::getValidAuthenticatedRequestHeadersForUser($user);
    }

    protected static function getValidAuthenticatedRequestHeadersForUser(User $user): array
    {
        $accessToken = self::getValidAccessTokenForUser($user);

        return [
            'Authorization' => "Bearer $accessToken"
        ];
    }

    protected static function getValidAccessTokenForUser(User $user): string
    {
        /** @var Factory $tokenJwtFactory */
        $tokenJwtFactory = app(Factory::class);

        return $tokenJwtFactory->generateAccessTokenForUser($user);
    }
}