<?php

namespace Tests\Http\Controllers\AuthenticationController;

use Carbon\Carbon;
use Firebase\JWT\JWT;
use Symfony\Component\HttpFoundation\Response;
use Tests\Service\UserToken\TestCase;
use Tests\Traits\InteractsWithDatabase;
use Tests\Traits\UsesUsers;

class LogoutTest extends TestCase
{
    use InteractsWithDatabase;
    use UsesUsers;

    public static function setUpBeforeClass(): void
    {
        JWT::$timestamp = Carbon::parse('2024-02-11 23:14:00')->getTimestamp();
    }

    public static function tearDownAfterClass(): void
    {
        JWT::$timestamp = null;
    }

    public function tearDown(): void
    {
        $this->deleteUsers();

        parent::tearDown();
    }

    public function testGet()
    {
        $response = $this->get('/logout');
        $response->assertResponseStatus(Response::HTTP_METHOD_NOT_ALLOWED);
    }

    public function testPostNoParams()
    {
        $response = $this->post('/logout');
        $response->assertResponseStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testPostInvalid()
    {
        $response = $this->post('/logout', [
            'refresh_token' => 'invalid'
        ]);
        $response->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testPostValid()
    {
        $this->setUpUsers();

        $validRefreshToken = $this->getValidRefreshToken();

        //First call should be successfull.
        $response = $this->post('/logout', [
            'refresh_token' => $validRefreshToken
        ]);
        $response->assertResponseStatus(Response::HTTP_NO_CONTENT);

        //Second call should fail.
        $response = $this->post('/logout', [
            'refresh_token' => $validRefreshToken
        ]);
        $response->assertResponseStatus(Response::HTTP_UNAUTHORIZED);

        $this->assertDatabaseHas('invalidated_refresh_tokens', [
            'token' => $validRefreshToken,
        ]);
    }
}