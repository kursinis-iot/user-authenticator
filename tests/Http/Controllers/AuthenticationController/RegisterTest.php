<?php

namespace Tests\Http\Controllers\AuthenticationController;

use App\Models\User;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;
use Tests\Traits\InteractsWithDatabase;

class RegisterTest extends TestCase
{
    use InteractsWithDatabase;

    protected function tearDown(): void
    {
        User::query()->delete();
    }

    public function testGet()
    {
        $response = $this->get('/register');
        $response->assertResponseStatus(Response::HTTP_METHOD_NOT_ALLOWED);
    }

    public function testPostNoParams()
    {
        $response = $this->post('/register');
        $response->assertResponseStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testPostNoFirstName()
    {
        $response = $this->post('/register', [
            'last_name' => 'Kraujelis',
            'email' => 'karolis@kudze.lt',
            'password' => 'testtest',
            'language' => User::USER_LANGUAGE_LT,
        ]);
        $response->assertResponseStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testPostNoLastName()
    {
        $response = $this->post('/register', [
            'first_name' => 'Karolis',
            'email' => 'karolis@kudze.lt',
            'password' => 'testtest',
            'language' => User::USER_LANGUAGE_LT,
        ]);
        $response->assertResponseStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testPostNoEmail()
    {
        $response = $this->post('/register', [
            'first_name' => 'Karolis',
            'last_name' => 'Kraujelis',
            'password' => 'testtest',
            'language' => User::USER_LANGUAGE_LT,
        ]);
        $response->assertResponseStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testPostInvalidEmail()
    {
        $response = $this->post('/register', [
            'first_name' => 'Karolis',
            'last_name' => 'Kraujelis',
            'email' => 'i am not valid email',
            'password' => 'testtest',
            'language' => User::USER_LANGUAGE_LT,
        ]);
        $response->assertResponseStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testPostNoPassword()
    {
        $response = $this->post('/register', [
            'first_name' => 'Karolis',
            'last_name' => 'Kraujelis',
            'email' => 'karolis@kudze.lt',
            'language' => User::USER_LANGUAGE_LT,
        ]);
        $response->assertResponseStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testPostNoLanguage()
    {
        $response = $this->post('/register', [
            'first_name' => 'Karolis',
            'last_name' => 'Kraujelis',
            'email' => 'karolis@kudze.lt',
        ]);
        $response->assertResponseStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testPostInvalidLanguage()
    {
        $response = $this->post('/register', [
            'first_name' => 'Karolis',
            'last_name' => 'Kraujelis',
            'email' => 'karolis@kudze.lt',
            'language' => 'ru',
        ]);
        $response->assertResponseStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testPost()
    {
        $response = $this->post('/register', [
            'first_name' => 'Karolis',
            'last_name' => 'Kraujelis',
            'email' => 'karolis@kudze.lt',
            'password' => 'testtest',
            'language' => User::USER_LANGUAGE_LT,
        ]);

        $response->assertResponseStatus(Response::HTTP_NO_CONTENT);

        $this->assertDatabaseHas('users', [
            'email' => 'karolis@kudze.lt',
            'first_name' => 'Karolis',
            'last_name' => 'Kraujelis',
            'language' => User::USER_LANGUAGE_LT,
        ]);

        $this->assertDatabaseMissing('users', [
            'email' => 'karolis@kudze.lt',
            'email_verification_code' => null,
        ]);
    }

}
