<?php

namespace Tests\Http\Controllers\AuthenticationController;

use App\Models\User;
use App\Service\PasswordHasher;
use Carbon\Carbon;
use Firebase\JWT\JWT;
use Symfony\Component\HttpFoundation\Response;
use Tests\Service\UserToken\TestCase;
use Tests\Traits\UsesUsers;

class RefreshTokenTest extends TestCase
{
    use UsesUsers;

    public static function setUpBeforeClass(): void
    {
        JWT::$timestamp = Carbon::parse('2024-02-11 23:14:00')->getTimestamp();
    }

    public static function tearDownAfterClass(): void
    {
        JWT::$timestamp = null;
    }

    public function tearDown(): void
    {
        User::query()->delete();

        parent::tearDown();
    }

    public function testGet()
    {
        $response = $this->get('/refresh');
        $response->assertResponseStatus(Response::HTTP_METHOD_NOT_ALLOWED);
    }

    public function testPostNoParams()
    {
        $response = $this->post('/refresh');
        $response->assertResponseStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testPostInvalidToken()
    {
        $this->setUpUsers();

        $response = $this->post('/refresh', [
            'refresh_token' => 'not valid token'
        ]);
        $response->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testPostNoUser()
    {
        $response = $this->post('/refresh', [
            'refresh_token' => $this->getValidRefreshToken()
        ]);
        $response->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testPostAccessToken()
    {
        $this->setUpUsers();

        $response = $this->post('/refresh', [
            'refresh_token' => $this->getValidAccessToken()
        ]);
        $response->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testPost()
    {
        $this->setUpUsers();

        $response = $this->post('/refresh', [
            'refresh_token' => $this->getValidRefreshToken()
        ]);
        $response->assertResponseStatus(Response::HTTP_OK);
    }

}