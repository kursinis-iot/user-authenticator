<?php

namespace Tests\Http\Controllers\AuthenticationController;

use Symfony\Component\HttpFoundation\Response;
use Tests\Service\UserToken\TestCase;
use Tests\Traits\InteractsWithDatabase;
use Tests\Traits\UsesUsers;

class LoginTest extends TestCase
{
    use InteractsWithDatabase;
    use UsesUsers;

    public function tearDown(): void
    {
        $this->deleteUsers();

        parent::tearDown();
    }

    public function testGet()
    {
        $response = $this->get('/login');
        $response->assertResponseStatus(Response::HTTP_METHOD_NOT_ALLOWED);
    }

    public function testPostNoParams()
    {
        $response = $this->post('/login');
        $response->assertResponseStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testPostNoEmail()
    {
        $response = $this->post('/login', [
            'email' => 'test@gmail.com',
        ]);
        $response->assertResponseStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testPostNoPassword()
    {
        $response = $this->post('/login', [
            'password' => 'testtest'
        ]);
        $response->assertResponseStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testPostIncorrectEmail()
    {
        $this->setUpUsers();

        $response = $this->post('/login', [
            'email' => 'test1@gmail.com',
            'password' => 'testtest'
        ]);
        $response->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testPostIncorrectPassword()
    {
        $this->setUpUsers();

        $response = $this->post('/login', [
            'email' => 'test@gmail.com',
            'password' => 'testtest1'
        ]);
        $response->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testPostInactiveAccount()
    {
        $this->setUpUsers();

        $response = $this->post('/login', [
            'email' => 'test2@gmail.com',
            'password' => 'testtest'
        ]);
        $response->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testPost()
    {
        $this->setUpUsers();

        $response = $this->post('/login', [
            'email' => 'test@gmail.com',
            'password' => 'testtest'
        ]);
        $response->assertResponseStatus(Response::HTTP_OK);
    }
}
