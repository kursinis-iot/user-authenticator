<?php

namespace Tests\Http\Controllers\AuthenticationController;

use App\Models\User;
use App\Service\PasswordHasher;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;
use Tests\Traits\InteractsWithDatabase;

class VerifyEmailTest extends TestCase
{
    use InteractsWithDatabase;

    public function setUpUsers(): void
    {
        /** @var PasswordHasher $passwordHasher */
        $passwordHasher = app(PasswordHasher::class);

        User::factory()->createMany([
            [
                'email' => 'test@gmail.com',
                'password_hash' => $passwordHasher->hashPassword('testtest'),
                'email_verification_code' => null
            ], [
                'email' => 'test2@gmail.com',
                'password_hash' => $passwordHasher->hashPassword('testtest'),
                'email_verification_code' => 'a1b2c3d4e5'
            ]
        ]);
    }

    public function tearDown(): void
    {
        User::query()->delete();

        parent::tearDown();
    }

    public function testGet()
    {
        $response = $this->get('/verify');
        $response->assertResponseStatus(Response::HTTP_METHOD_NOT_ALLOWED);
    }

    public function testPostNoParams()
    {
        $response = $this->post('/verify');
        $response->assertResponseStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testPostNoEmail()
    {
        $response = $this->post('/verify', [
            'code' => 'a1b2c3d4e5'
        ]);
        $response->assertResponseStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testPostNoCode()
    {
        $response = $this->post('/verify', [
            'email' => 'test2@gmail.com',
        ]);
        $response->assertResponseStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testPostInvalidEmail()
    {
        $this->setUpUsers();
        $response = $this->post('/verify', [
            'email' => 'test2456@gmail.com',
            'code' => 'a1b2c3d4e5'
        ]);
        $response->assertResponseStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testPostInvalidCode()
    {
        $this->setUpUsers();
        $response = $this->post('/verify', [
            'email' => 'test2@gmail.com',
            'code' => '0123456789'
        ]);
        $response->assertResponseStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testPostAccountActivated()
    {
        $this->setUpUsers();
        $response = $this->post('/verify', [
            'email' => 'test@gmail.com',
            'code' => 'a1b2c3d4e5'
        ]);
        $response->assertResponseStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testPost()
    {
        $this->setUpUsers();
        $response = $this->post('/verify', [
            'email' => 'test2@gmail.com',
            'code' => 'a1b2c3d4e5'
        ]);

        $response->assertResponseStatus(Response::HTTP_NO_CONTENT);

        $this->assertDatabaseHas('users', [
            'email' => 'test2@gmail.com',
            'email_verification_code' => null
        ]);
    }
}