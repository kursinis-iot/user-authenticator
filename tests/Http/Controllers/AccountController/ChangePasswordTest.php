<?php

namespace Tests\Http\Controllers\AccountController;

use App\Exceptions\InvalidCredentialsException;
use App\Exceptions\UserNotVerifiedException;
use App\Service\UserService;
use Symfony\Component\HttpFoundation\Response;
use Tests\Service\UserToken\TestCase;
use Tests\Traits\UsesJWT;
use Tests\Traits\UsesUsers;


class ChangePasswordTest extends TestCase
{
    use UsesUsers;
    use UsesJWT;

    public function setUp(): void
    {
        parent::setUp();

        self::setUpUsers();
    }

    public function tearDown(): void
    {
        self::deleteUsers();

        parent::tearDown();
    }

    public function testNoJWT()
    {
        $this->post('/password', [
            'old_password' => 'test',
            'new_password' => 'test',
        ])->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testInvalidJWT()
    {
        $this->post('/password', [
            'old_password' => 'test',
            'new_password' => 'test',
        ], [
            'Authorization' => 'Bearer test'
        ])->assertResponseStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testNotSecurePassword()
    {
        $this->post(
            '/password',
            [
                'old_password' => 'testtest',
                'new_password' => 'test',
            ],
            $this->getValidAuthenticatedRequestHeadersForEmail('test@gmail.com')
        )->assertResponseStatus(422);
    }

    /**
     * @throws InvalidCredentialsException
     * @throws UserNotVerifiedException
     */
    public function testValid()
    {
        $email = 'test@gmail.com';
        $newPassword = 'verysecurepassword';

        $this->post(
            '/password',
            [
                'old_password' => 'testtest',
                'new_password' => $newPassword,
            ],
            $this->getValidAuthenticatedRequestHeadersForEmail($email)
        )->assertResponseStatus(204);

        /** @var UserService $userService */
        $userService = app(UserService::class);
        $user = $userService->findUserByCredentials($email, $newPassword);

        $this->assertSame($email, $user->getAttribute('email'));
    }
}