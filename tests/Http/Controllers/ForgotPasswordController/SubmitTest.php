<?php

namespace Tests\Http\Controllers\ForgotPasswordController;

use App\Models\ForgotPasswordToken;
use App\Models\User;
use Tests\Service\UserToken\TestCase;

class SubmitTest extends TestCase
{
    public function tearDown(): void
    {
        User::query()->delete();

        parent::tearDown();
    }

    public function testNoParams()
    {
        $this->post('/password/forgot/submit')->assertResponseStatus(422);
    }

    public function testNoEmail()
    {
        $this->post('/password/forgot/submit', [
            'code' => 'test',
            'password' => 'testtest',
        ])->assertResponseStatus(422);
    }

    public function testInvalidEmail()
    {
        $this->post('/password/forgot/submit', [
            'email' => 'test',
            'code' => 'test',
            'password' => 'testtest',
        ])->assertResponseStatus(422);
    }

    public function testNotExistingEmail()
    {
        $this->post('/password/forgot/submit', [
            'email' => 'test@gmail.com',
            'code' => 'test',
            'password' => 'testtest',
        ])->assertResponseStatus(422);
    }

    public function testInvalidCode_noTokenRequested()
    {
        User::factory()->createOne([
            'email' => 'test@gmail.com'
        ]);

        $this->post('/password/forgot/submit', [
            'email' => 'test@gmail.com',
            'code' => 'test',
            'password' => 'testtest',
        ])->assertResponseStatus(422);
    }

    public function testInvalidCode_reallyInvalidCode()
    {
        User::factory()->createOne([
            'uuid' => '1',
            'email' => 'test@gmail.com'
        ]);

        ForgotPasswordToken::factory()->createOne([
            'user_uuid' => '1',
            'token' => 'somethingE'
        ]);

        $this->post('/password/forgot/submit', [
            'email' => 'test@gmail.com',
            'code' => 'test',
            'password' => 'testtest',
        ])->assertResponseStatus(422);
    }

    public function testNoPassword()
    {
        User::factory()->createOne([
            'uuid' => '1',
            'email' => 'test@gmail.com'
        ]);

        ForgotPasswordToken::factory()->createOne([
            'user_uuid' => '1',
            'token' => 'test'
        ]);

        $this->post('/password/forgot/submit', [
            'email' => 'test@gmail.com',
            'code' => 'test',
        ])->assertResponseStatus(422);
    }

    public function testInvalidPassword()
    {
        User::factory()->createOne([
            'uuid' => '1',
            'email' => 'test@gmail.com'
        ]);

        ForgotPasswordToken::factory()->createOne([
            'user_uuid' => '1',
            'token' => 'test'
        ]);

        $this->post('/password/forgot/submit', [
            'email' => 'test@gmail.com',
            'code' => 'test',
            'password' => 'test',
        ])->assertResponseStatus(422);
    }

    public function testValid()
    {
        User::factory()->createOne([
            'uuid' => '1',
            'email' => 'test@gmail.com'
        ]);

        ForgotPasswordToken::factory()->createOne([
            'user_uuid' => '1',
            'token' => 'test'
        ]);

        $this->post('/password/forgot/submit', [
            'email' => 'test@gmail.com',
            'code' => 'test',
            'password' => 'testtest'
        ])->assertResponseStatus(204);
    }
}