<?php

namespace Tests\Http\Controllers\ForgotPasswordController;

use App\Exceptions\InvalidCredentialsException;
use App\Exceptions\UserNotVerifiedException;
use App\Service\UserService;
use Symfony\Component\HttpFoundation\Response;
use Tests\Service\UserToken\TestCase;
use Tests\Traits\UsesUsers;


class InitiateTest extends TestCase
{
    use UsesUsers;

    public function setUp(): void
    {
        parent::setUp();

        self::setUpUsers();
    }

    public function tearDown(): void
    {
        self::deleteUsers();

        parent::tearDown();
    }

    public function testInvalidEmail()
    {
        $this->post('/password/forgot/initiate', [
            'email' => 'test',
        ])->assertResponseStatus(422);
    }

    public function testNotExistingEmail()
    {
        $this->post('/password/forgot/initiate', [
            'email' => 'notexisting@gmail.com',
        ])->assertResponseStatus(204);
    }

    public function testValid()
    {
        $this->post('/password/forgot/initiate', [
            'email' => 'test@gmail.com',
        ])->assertResponseStatus(204);
    }
}