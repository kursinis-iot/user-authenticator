<?php /** @noinspection GrazieInspection */

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->post('/register', [
    'as' => 'register',
    'uses' => 'AuthenticationController@register'
]);

$router->post('/login', [
    'as' => 'login',
    'uses' => 'AuthenticationController@login'
]);

$router->post('/refresh', [
    'as' => 'refresh',
    'uses' => 'AuthenticationController@refreshToken'
]);

$router->post('/verify', [
    'as' => 'verifyEmail',
    'uses' => 'AuthenticationController@verifyEmail'
]);

$router->post('/logout', [
    'as' => 'logout',
    'uses' => 'AuthenticationController@logout'
]);

$router->post('/password/forgot/initiate', [
    'as' => 'forgotPasswordInitiate',
    'uses' => 'ForgotPasswordController@initiate'
]);

$router->post('/password/forgot/submit', [
    'as' => 'forgotPasswordSubmit',
    'uses' => 'ForgotPasswordController@submit'
]);

$router->group(['middleware' => 'auth'], function () use ($router) {
    $router->post('/password', 'AccountController@changePassword');

});